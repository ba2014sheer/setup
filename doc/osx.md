
# OSX tips

## Bouncing

From [this article](https://www.switchingtomac.com/tutorials/how-to-permanently-stop-dock-icons-from-bouncing/):
```
defaults write com.apple.dock no-bouncing -bool TRUE
```

## Other

Found in some bash profile online: 
```bash
# Fix audio control issues
alias fix-audio='sudo launchctl unload /System/Library/LaunchDaemons/com.apple.audio.coreaudiod.plist && sudo launchctl load /System/Library/LaunchDaemons/com.apple.audio.coreaudiod.plist'

# Add a spacer to the dock
alias add-dock-spacer='defaults write com.apple.dock persistent-apps -array-add "{'tile-type'='spacer-tile';}" && killall Dock' 
```

