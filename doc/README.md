
# Bash setup library

This is a bash library originally designed to:
 - provide shell utilities that work on both Linux and OSX;
 - manage `git` repositories;
 - synchronise folders between machines (using `rsync`);
 - and manage `ssh` keys (in `~/.ssh`).

This mostly-bash library is designed to be "plug & play"; it doesn't mess with the host system, you just source it and use it, and nothing breaks when you unplug it.

It is compatible with both OSX and Linux (although [Homebrew](https://brew.sh/) is required on OSX), and has been tested on:

 - OSX Sierra, High Sierra, Mojave
 - Linux CentOS, RedHat and Ubuntu

## Trying it

If you just want to try it quickly, type:
```
git clone https://gitlab.com/jhadida/setup.git ~/.jh
source ~/.jh/inc/profile.sh
```

This might spit out errors about packages not being installed, but your prompt should be colourful and start with a prefix `(none)`. 

You now have access to [many bash tools](./libs/bash.md) within your terminal. Note that the profile you just sourced comes with its own shell [options](https://gitlab.com/jhadida/setup/blob/master/src/profile/options.sh), [configuration](https://gitlab.com/jhadida/setup/blob/master/src/profile/config.sh) and [aliases](https://gitlab.com/jhadida/setup/blob/master/src/profile/alias.sh); although these only affect the current shell, they might differ from your usual preferences. 

## Issues and contribution

Everything happens on the [GitLab repo](https://gitlab.com/jhadida/setup). Open an issue if you find something that doesn't work, or if you have suggestions for improvements.
