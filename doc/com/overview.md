
# Overview

## Structure of the repo

```
bin/
    Contains various tools to be used by all users.
    If your .bashrc or .bash_profile sources inc/profile.sh, then this folder is added to the path.

doc/
    Sources for this documentation.
    Can be served using: docsify serve "$JH_DOC"

etc/
    Configuration stuff, currently contains lists of software packages.

inc/
    Files to be sourced manually from bash scripts.
    For example: source "$JH_INC/bashlib.sh"

src/
    Code of all libraries.
    Used by the tools, not intended to be used manually.

tpl/
    Various templates used by the tool jhtpl.

usr/
    Everything that is user-specific.
    The following folders should be common to all users: 
        bin app repo host data
    See JH_USER_FOLDERS in src/jh/user-utils.sh

var/
    All the runtime things. This folder is excluded from version control.
```

## Basic usage

You should add this library to your `.bash_profile` or `.bashrc`. 
If you didn't do so during installation, then just run: `jhtpl profile`

The main benefits of this repo are:

 - Lots of useful bash tools available in your shell (see the [bash library](libs/bash.md));
 - Powerful tools to manage data and code repositories (see the [jh library](libs/jh.md));
 - Designed to be forked by users to easily synchronise their config across machines.
