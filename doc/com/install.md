
# Installation

First, define the following (edit as needed):
```bash
UserName=$USER
Folder=$HOME/.jh
```

Then run these commands:
```bash
# clone repo
git clone git@gitlab.com:jhadida/setup.git "$Folder"
    or
git clone https://gitlab.com/jhadida/setup.git "$Folder"

# deploy profile helper
source "$Folder/inc/jhenv.sh"
"$Folder/bin/jhtpl" profile
    # this will prompt you to add something to your .bashrc

# create user
"$Folder/bin/jhusr" $UserName
```

For the documentation, install Docsify: `npm i -g docsify-cli`
