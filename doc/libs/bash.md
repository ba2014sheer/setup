
# Bash library

## Core functions

Checkout the following files:
```
src/bash/preload.sh
src/bash/common.sh
src/bash/colors.sh
```

## Utilities

File manip (`src/bash/util_file`)
```
fileswap
abspath
absdir
unlink
relink

dirsize
dirfree
dirempty

backup
backfile
removedir
trash

fileline
iterlines
lcmp

win2nix
nix2win
```

Find things (`src/bash/util_find`)
```
fstr
frep
fstr_(list|file)
deadlinks
```

Archive & crypto (`src/bash/util_archive`)
```
compress
extract
tarhealth
```

System stuff  (`src/bash/util_path, src/bash/util_system`)
```
path_(prepend|remove|print)
lib_(prepend|remove|print)
ncpus
loadavg
linux-distro
```

Repo stuff (`src/bash/util_repo`)
```
gits|gitn
git_(remote|showup|upstream|curbranch|listbranch|lsbr|chkroot|mtime)
```


## Application helpers

PDF (`src/bash/app_pdf`)
```
pdf_extract
pdf2img
```

TeX (`src/bash/app_tex`)
```
ctex
cbtex
```

Crypto (`src/bash/app_crypto`)
```
randhash
fencrypt/fdecrypt
```


Other (`src/bash/app_other`)
```
weather
url_reachable
```