
# JH library

## Repository manager

See documentation for `jhr`.

Repo config is stored in `$JH_USR/repo`. Three kinds of files:

 - `.vcs` version control systems (GitHub, GitLab, etc.)
 - `.conf` configuration for a particular repo
 - `.lst` sources for a particular repo

Each repo has `.conf` and `.lst` files. 
The `.lst` file points to multiple `.vcs` files.
The `.vcs` files are user-specific; for example `github-jhadida.vcs` and `github-sheljohn.vcs`.

## Data manager

See documentation for `jhd`.

Data config is stored in `$JH_USR/data`. 

Data sources are configured at three different levels:

 - A remote data source (`.rds` files) is defined by a triplet user/host/path.
 - Aliases can be created (stored in `$JH_VAR/data/alias`), to extend the path behind an existing remote source.
 - Finally, each alias can be linked to a local folder; the mappings are stored in `$JH_VAR/data/link`.

## SSH helpers

See documentation for `jhs`.

Host configuration is stored in `$JH_USR/host`.

## Software installers

See documentation for `jhinst`.

The actual installers are stored in `$JH_SRC/inst`.

## User management

See documentation for `jhusr`.

Unused users are stored in `$JH_VAR/user`.
