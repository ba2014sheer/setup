
if [ -z "$JH_ROOT" ]; then 
    echo "Variable JH_ROOT unset."
    echo "Environment should be sourced before including the profile."
    return 1
fi

# directory where this script lives
HERE=$( cd "$( dirname "${BASH_SOURCE}" )" && pwd -P )
if [ -n "$JH_FLAG_BASH" ]; then

    # shell options
    source "${HERE}/options.sh"

    # source utilities
    source "${JH_SRC}/bash/include.sh"

    # preferences
    source "${HERE}/alias.sh"
    source "${HERE}/completion.sh"
    source "${HERE}/config.sh"
    source "${HERE}/osx.sh"
    # source "${HERE}/fmrib.sh"

    # source prompt
    source "${HERE}/prompt.sh"

fi
