
# source system completions
BC='/etc/bash_completion'; [ -f "$BC" ] && source "$BC" # System
BC='/usr/local/etc/bash_completion.d'; [ -f "$BC" ] && source "$BC" # Homebrew
# /usr/share/bash-completion/bash_completion

complete -A hostname   rsh rcp telnet rlogin ftp ping disk
complete -A export     printenv
complete -A variable   export local readonly unset
complete -A enabled    builtin
complete -A alias      alias unalias
complete -A function   function
complete -A user       su mail finger

complete -A helptopic  help     # Currently same as builtins.
complete -A shopt      shopt
complete -A stopped -P '%' bg
complete -A job -P '%' fg jobs disown

complete -A directory  mkdir rmdir
complete -A directory  -o default cd

# Compression
complete -f -o default -X '*.+(zip|ZIP)'  zip
complete -f -o default -X '!*.+(zip|ZIP)' unzip
complete -f -o default -X '*.+(z|Z)'      compress
complete -f -o default -X '!*.+(z|Z)'     uncompress
complete -f -o default -X '*.+(gz|GZ)'    gzip
complete -f -o default -X '!*.+(gz|GZ)'   gunzip
complete -f -o default -X '*.+(bz2|BZ2)'  bzip2
complete -f -o default -X '!*.+(bz2|BZ2)' bunzip2
complete -f -o default -X '!*.+(zip|ZIP|z|Z|gz|GZ|bz2|BZ2)' extract

# Documents
complete -f -o default -X '!*.+(ps|PS)' gs ghostview ps2pdf ps2ascii
complete -f -o default -X '!*.+(dvi|DVI)' dvips dvipdf xdvi dviselect dvitype
complete -f -o default -X '!*.+(pdf|PDF)' evince pdf2ps
complete -f -o default -X '!*.@(@(?(e)ps|?(E)PS|pdf|PDF)?(.gz|.GZ|.bz2|.BZ2|.Z))' gv ggv
complete -f -o default -X '!*.texi*' makeinfo texi2dvi texi2html texi2pdf
complete -f -o default -X '!*.tex' tex latex slitex pdflatex
complete -f -o default -X '!*.+(htm*|HTM*)' html2ps
complete -f -o default -X '!*.+(doc|DOC|xls|XLS|ppt|PPT|sx?|SX?|csv|CSV|od?|OD?|ott|OTT)' soffice


#-------------------------------------------------------------
# Universal completion function.
# It works when commands have so-called "long-options":
# eg: 'ls --all' instead of 'ls -a'
#-------------------------------------------------------------

# First, remove '=' from completion word separators
# (this will allow completions like 'ls --color=auto' to work correctly).
COMP_WORDBREAKS=${COMP_WORDBREAKS/=/}

_get_longopts()
{
    $1 --help | grep -o -e "--[^[:space:].,]*" | grep -e "$2" | sort -u

    # alternative
    #$1 --help | sed  -e '/--/!d' -e 's/.*--\([^[:space:].,]*\).*/--\1/'| grep ^"$2" |sort -u ;
}

_longopts()
{
    local cur
    cur=${COMP_WORDS[COMP_CWORD]}

    case "${cur:-*}" in
       -*)      ;;
        *)      return ;;
    esac

    case $1 in
       \~*)     eval cmd=$1 ;;
         *)     cmd=$1 ;;
    esac
    COMPREPLY=( $(_get_longopts ${1} ${cur} ) )
}
complete -o default -F _longopts configure bash
complete -o default -F _longopts wget id info a2ps ls recode

# completion for many Bash commands
if [ -n "$(iscmd brew)" ]; then 
    BREW_PREFIX=$(brew --prefix)
    BREW_FILES=(
        'share/bash-completion/bash_completion'
        'etc/bash_completion.d/'*
    )
    for f in "${BREW_FILES[@]}"; do
        [ -f "${BREW_PREFIX}/$f" ] && source "${BREW_PREFIX}/$f"
    done
fi

# system-specific completions
case $JH_OS in
    OSX)
        complete -o "nospace" -W "Contacts Calendar Dock Finder Mail Safari iTunes SystemUIServer Terminal" killall
        ;;
    Linux)
        
        ;;
esac

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -f "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')" scp sftp ssh

