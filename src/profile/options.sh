
# Shell options
# autocd					`dir/name`+Enter => `cd dir/name`
# cdable_vars				`cd foo` => `cd $foo` if foo is not a dirname
# cdspell					typos in `cd` args are corrected on the fly
#
# checkhash					if a command hash is no longer valid, update hash table and search path
# checkwinsize				update LINES and COLUMNS after each command
#
# cmdhist					enable history
# histappend				append instead of overwrite history file
# histverify				load history command to the readline, instead of executing it directly
# histreedit				history substitution can fail but remains in the readline buffer to be edited
#
# dotglob					include filename begining with `.` in filename expansion
# extglob					extended pattern matching features
# extquote					?
# failglob					throw error if path name expansion fails
# nocaseglob				case-insensitive filename match
# nullglob					expand to null string when there's no match
#
# nocasematch				case-insensitive conditions like [[]] or `case`
# extdebug					execute commands in debug mode
# execfail					prevent non-interactive shell to exit on failure
#
# force_fignore				ignore suffixes listed in FIGNORE during completion, eg ".o:~"
# gnu_errfmt				standard GNU error format
# hostcomplete				complete hostnames (following a @)
# huponexit					send SIGHUP to active jobs on exit
# interactive_comments		ignore anything after a # until newline
# lithist					use newlines instead of ; for multi-line commands in history
# login_shell				internally used for login shells
# mailwarn					notify when watched file is edited
# no_empty_cmd_completion	don't search path for auto-complete an empty line
# progcomp					enable `complete`
# promptvars				expand the promp string
# restricted_shell			see http://www.gnu.org/software/bash/manual/html_node/The-Restricted-Shell.html
# shift_verbose				print error if shift count exceeds $#
# sourcepath				search PATH as well to source the file
# xpg_echo					expand basckslash=escape sequences in `echo`

# GNU-specific shell options
# checkjobs					off
# compat31					off
# compat32					off
# compat40					off
# compat41					off
# compat42					off
# complete_fullquote		on
# direxpand					off
# dirspell					off
# expand_aliases			on
# globasciiranges			off
# globstar					off
# lastpipe					off

# Set options:
# -a [allexport] export all following variables/functions
# -n [noexec] just read, don't run the commands
# -x [xtrace] turn on expanded echo
# -B [braceexpand] turn on brace expansion
# -u [nounset] fail on unset variable substitution
# -h [hashall] enable command hashing for fast lookup

set -o noclobber # use >| to overwrite existing files
set +o ignoreeof # Ctrl+D is not ignored to exit the terminal
stty -ixon # ignore scroll-locks caused by Ctrl-S / Ctrl-Q

shopt -s extglob # needed for completion?
shopt -s sourcepath
shopt -s no_empty_cmd_completion
shopt -s checkhash checkwinsize
shopt -s cmdhist
shopt -s cdspell
shopt -s histappend histreedit histverify
shopt -u cdable_vars
shopt -u nocaseglob nocasematch 

case $JH_OS in 
    Linux)
        # annoying bug on some GNU/Linux systems, see: https://stackoverflow.com/a/18637561/472610
        shopt -s direxpand
        ;;
esac
