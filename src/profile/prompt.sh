
# SSH_CONNECTION = src:ip src:port dst:ip dst:port
SSH_CLIENT_NAME=''
if [ -n "$SSH_CONNECTION" ]; then

    # extract ip
    CLIENT_IP=($SSH_CONNECTION)
    CLIENT_IP=${CLIENT_IP[0]}

    # get hostname from DNS lookup
    SSH_CLIENT_NAME=$(host "$CLIENT_IP" | awk '{print $NF}' | sed 's/\.*$//')
    
fi

# Get the current TTY
_get_tty() {
    local tmp=$(tty)
    echo ${tmp:5}
}

# Get type of shell we are running (local, telnet or ssh)
# cf http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/x865.html
_get_shell_type() {
    local this_tty=tty$`_get_tty`
    local sess_src=$(who | grep $this_tty | awk '{print $6}')
    local ssh_flag=0

    if [ $(echo $SSH_CLIENT | awk '{print $1}') ]; then
        ssh_flag=1
    elif [ $(echo $SSH2_CLIENT | awk '{print $1}') ]; then
        ssh_flag=1
    fi

    if [ $ssh_flag -eq 1 ]; then
        echo "ssh"
    elif [ -z $sess_src ]; then
        echo "local"
    elif [ $sess_src = "(:0.0)" -o $sess_src = "" ] ; then
        echo "local"
    else
        echo "telnet"
    fi
}

#-------------------------------------------------------------
# PROMPT
#-------------------------------------------------------------

prompt_load_color() {
    local load=($(loadavg)) # util_system

    if [ ${load[1]} -gt 95 ]; then
        echo -en ${FontB}${ColM}
    elif [ ${load[0]} -gt 80 ]; then
        echo -en ${ColR}
    elif [ ${load[0]} -gt 60 ]; then
        echo -en ${ColY}
    else
        echo -en ${ColG}
    fi
}

prompt_user_color() {
    local user=$USER
    #local name=$(logname)
    local name=$LOGNAME

    if [[ "$user" == "root" ]]; then
        echo -en ${FontB}${ColY}
    elif [[ "$name" != "$user" ]]; then
        echo -en ${FontB}${ColR}
    else
        echo -en ${FontB}${ColC}
    fi
}

prompt_host_color() {
    local stype=$(_get_shell_type)

    if [[ $stype == "ssh" ]]; then
        echo -en ${FontB}${ColY}
    elif [[ $stype == "telnet" ]]; then
        echo -en ${FontB}${ColR}
    else
        echo -en ${FontB}${ColC}
    fi
}

# name prefix
if [[ $(isset JH_PROMPT_NONAME) ]]; then 
    PS1=""
elif [ -n "$JH_PROMPT_NAME" ]; then 
    PS1="[$JH_PROMPT_NAME] "
else
    PS1="(\$(JH_getUserName)) "
fi

# time of day (with load info):
if [[ $(isset JH_PROMPT_NOLOAD) ]]; then 
    PS1=${PS1}"[\t "
else 
    PS1=${PS1}"[\[\$(prompt_load_color)\]\t\[${FontD}\] "
fi

# user@host (with connection type info):
PS1=${PS1}"\[\$(prompt_user_color)\]\u\[${FontD}\]"
PS1=${PS1}"@\[\$(prompt_host_color)\]\h\[${FontD}\] "

# current working directory
PS1=${PS1}"\W]> "

# terminal window title
PS1=${PS1}"\[\033]0;\h: \w\a\]"

# dont export
#export PS1