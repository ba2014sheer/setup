
if [ "$JH_OS" = "OSX" ]; then

alias show-hidden-files='defaults write com.apple.finder AppleShowAllFiles 1 && killall Finder'
alias hide-hidden-files='defaults write com.apple.finder AppleShowAllFiles 0 && killall Finder'
    # Hidden files
alias show-dashboard='defaults write com.apple.dashboard mcx-disabled -boolean NO && killall Dock'
alias hide-dashboard='defaults write com.apple.dashboard mcx-disabled -boolean YES && killall Dock'
    # Dashboard
alias show-spotlight='sudo mdutil -a -i on'
alias hide-spotlight='sudo mdutil -a -i off'  
    # Spotlight
alias bouncing-off='defaults write com.apple.dock no-bouncing -bool TRUE && killall Dock'
alias bouncing-on='defaults write com.apple.dock no-bouncing -bool FALSE && killall Dock'
    # Bouncing icons

alias funfacts='grep -h -d skip `date +%m/%d` /usr/share/calendar/*'
    # Get history facts about the day

# Homebrew
alias brewup='brew update && brew upgrade && brew cleanup'
alias brewup-cask='brew update && brew upgrade && brew cleanup && brew cask outdated | awk "{print $1}" | xargs brew cask reinstall && brew cask cleanup'


# DEPRECATION IN FAVOUR OF PYTHON IMPLEMENTATION
# alias pdfmerge='/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py'                                  # Merge PDF files - Usage: `mergepdf -o output.pdf input{1,2,3}.pdf`

fi