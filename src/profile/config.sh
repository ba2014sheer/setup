
# UTF-8 terminal
export LC_CTYPE=en_GB.UTF-8
export LC_ALL=en_GB.UTF-8
export LANG=en_GB.UTF-8

# don't put duplicate lines into the history file
export HISTCONTROL=ignoreboth
export HISTIGNORE="&:?:l[sl]:[bf]g:clc:clear"
export HISTTIMEFORMAT="[%Y-%m-%d %H:%M:%S (%a %d %b)] "

# control size of history files
export HISTSIZE=10000
export HISTFILESIZE=100000

# system-specific configuration?
case $JH_OS in
OSX)
    # enable coloured outputs in OSX
    export CLICOLOR=1

    # colors for the ls command
    export LSCOLORS=GxFxCxDxBxegedabagaced
    ;;

Linux)
    ;;

esac

# On Linux or with gls
# see: https://gist.github.com/thomd/7667642
export LS_COLORS='di=1;34:fi=0:ex=32:ln=96:pi=4;33:so=4;35:bd=4;31:cd=4;93:or=5;31:mi=7;37'
