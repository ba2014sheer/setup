
# ask before overwriting/deleting
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

alias clc='clear'
	# make space in terminal
alias grep='grep --color=auto'
	# enable colors for terminal output
alias perm="stat -f '%Lp'" 
	# file/folder permission as a number

# change directory
alias ..="cd .."
alias ..2="cd ../.."
alias ..3="cd ../../.."
alias ..4="cd ../../../.."
alias ..5="cd ../../../../.."

case $JH_OS in
	OSX)
	# Sorting:
	# -S size
	# -t time modified
	# -tu time accessed
	# -tU time created

		# Requires: brew install coreutils
		if [[ $(iscmd gls) ]]; then
        	alias ls='LC_ALL=C gls -hH --color=auto --group-directories-first'
		else
			# try to work without Homebrew
			alias ls='ls -hG'
		fi
	;;
	Linux)
	# Sorting:
	# --sort=size
	# --sort=time modified
	#	--time=(access|ctime)
	# --sort=extension

		alias ls='LC_ALL=C ls -hH --color=auto --group-directories-first'

		# use open as in OSX
		alias open='xdg-open'
	;;
esac

# long and all listing
alias ll='ls -l'
alias la='ls -lA'
# sort by size or modification time (use -r for reversed variants)
alias lls='ll -S'
alias llt='ll -t'

# git shortcuts
alias git_amend='git commit --amend'
alias git_undo='git reset --soft HEAD~1'
alias git_pullsub='git submodule foreach git pull origin master'
alias git_log="git log --graph --abbrev-commit --decorate --all --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(dim white) - %an%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n %C(white)%s%C(reset)'"
alias git_chlog="git log --oneline --decorate --stat --graph --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(dim white) - %an%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n %C(white)%s%C(reset)'"
