
# ---------------------------------------------------------------------

# source utilities
source "${JH_SRC}/jh/common.sh"
FailBadge='data-utils'

# check that there is a current user
[ -d "$JH_USR" ] || fail "User folder not found. Did you use jhusr yet?"

# paths
JH_DATA_SOURCE="${JH_USR}/data"
JH_DATA_ALIAS="${JH_VAR}/data/alias"
JH_DATA_LINK="${JH_VAR}/data/link"

# make sure rsync is there
checkDependency 'rsync'

# ensure folder exists
ensureFolder "$JH_DATA_ALIAS"
ensureFolder "$JH_DATA_LINK"

# make sure exclude file is there
ExcludeFile="${JH_USR}/data/exclude"
checkFileOrCopy "$ExcludeFile" "${JH_ETC}/data/exclude"

# ---------------------------------------------------------------------

# remote file-test
remoteCheck() {
    echo $(ssh "${User}@${Server}" [ -d "$Folder" ] && echo 1; exit);
}

# print remote path
remotePath() {
    echo "${User}@${Server}:${Folder}"
}

# test and pretty-print remote status
remoteShowStatus() {
    if [ -n "$(remoteCheck)" ]; then 
        echo -e "$*${ColG}up${FontD}"
    else 
        echo -e "$*${FontB}${ColR}down${FontD}"
    fi
}

# ---------------------------------------------------------------------

# use paths defined above to form an absolute filepath
jhd_sourceFile() { echo "${JH_DATA_SOURCE}/${1%.rds}.rds"; }

# check whether data source file exists in the etc/ folder
jhd_isSource() { [ -f "$(jhd_sourceFile "$1")" ] && echo 1; }

# load data source information
# defines variables User Server Folder
#
#   User
#   Server
#   Folder      
#
jhd_loadSource() {
    if [[ $(jhd_isSource "$1") ]]; then 
        SourceFile=$(jhd_sourceFile "$1")
        source "$SourceFile"
    else 
        fail "Source not found: $1"
    fi
}

# loads source, then remote file-test
# use as [ -n "$(jhd_testSource foo)" ]
jhd_testSource() {
    jhd_loadSource "$1"
    echo $(remoteCheck)
}

# pretty-print of source status
# usage: showSourceStatus sourceName msgPrefix
jhd_showSourceStatus() {
    jhd_loadSource "$1"
    shift
    remoteShowStatus "$*"
}

# prints the remote path corresponding to the source
jhd_sourceTarget() {
    jhd_loadSource "$1"
    remotePath
}

# ---------------------------------------------------------------------

# use paths defined above to form an absolute filepath
jhd_aliasFile() { echo "${JH_DATA_ALIAS}/$1"; }

# check whether data alias file exists in the /var folder
jhd_isAlias() { [ -f "$(jhd_aliasFile "$1")" ] && echo 1; }

# load alias information
# defines variables User Server Folder and SubFolder
# Folder is edited to Folder/SubFolder
#
#   User
#   Server
#   Folder      
#   Subfolder
#
# NOTE: prop Folder is edited to reflect the alias
#   Folder   <-   Folder/Subfolder
#
jhd_loadAlias() {
    if [[ $(jhd_isAlias "$1") ]]; then 
        AliasFile=$(jhd_aliasFile "$1")
        source "$AliasFile"
        jhd_loadSource "$Source"
        [ -n "$SubFolder" ] && Folder="${Folder}/${SubFolder}" # subfolder can be empty
    else 
        fail "Alias not found: $1"
    fi
}

# prints the remote path corresponding to the alias
jhd_aliasTarget() {
    jhd_loadAlias "$1"
    remotePath
}

# load alias, then remote file-test
# use as [ -n "$(jhd_testAlias foo)" ]
jhd_testAlias() {
    jhd_loadAlias "$1"
    echo $(remoteCheck)
}

# pretty-print of alias status
# usage: showAliasStatus aliasName msgPrefix
jhd_showAliasStatus() {
    jhd_loadAlias "$1"
    shift
    remoteShowStatus "$*"
}

# show remote address corresponding to alias
jhd_showAliasRemote() {
    jhd_loadAlias "$1"
    shift
    echo -e "$*$(remotePath)"
}

# ---------------------------------------------------------------------

# use paths defined above to form an absolute filepath
jhd_linkFile() { echo "${JH_DATA_LINK}/$1"; }

# check whether link exists in the /var folder
jhd_isLink() { [ -L "$(jhd_linkFile "$1")" ] && echo 1; }

# check if link exist and is not broken
#   0   all good
#   1   link does not exist
#   2   link is broken
jhd_checkLink() {
    local File=$(jhd_linkFile "$1")
    local Target=$(readlink "$File")

    if [ ! -L "$File" ]; then
        echo 1
    elif [ ! -d "$Target" ]; then 
        echo 2 
    else 
        echo 0
    fi
}

# pretty-print of link target
# usage: showLinkTarget aliasName msgPrefix
jhd_showLinkTarget() {
    local File=$(jhd_linkFile "$1")
    local Target=$(readlink "$File")
    shift 

    if [ ! -L "$File" ]; then
        echo -e "$*${ColW}-${FontD}"
    elif [ ! -d "$Target" ]; then 
        echo -e "$*${FontB}${ColM}${Target}${FontD}"
    else 
        echo -e "$*${ColG}${Target}${FontD}"
    fi
}

# read target of link, error if link is bad
jhd_linkTarget() {
    local File=$(jhd_linkFile "$1")
    local Target=$(readlink "$File")

    [ ! -L "$File" ] && fail "Link not found: $1"
    [ ! -d "$Target" ] && fail "Broken link: $1"
    echo "$Target"
}
