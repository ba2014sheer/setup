# to be sourced by common.sh

fixIssues() {
    :
    # put issues to be fixed here
}

issueInfo() { msg_Y "[jh/issues] $*"; }

# ------------------------------------------------------------------------

issueUserEnv() {
    OldUserName="$JH_ROOT/usr/.name"
    NewUserEnv="$JH_ROOT/usr/env.sh"

    if [ ! -e "$NewUserEnv" ]; then 
        issueInfo "Fixing issue: UserEnv"
        checkFile "$OldUserName"

        UserName=$(cat "$OldUserName")
        msg_bR "User config has changed!"
        msg_R "\tYou will now be asked to edit a configuration file in vim:"
        cp -f "$JH_TPL/jh/env" "$NewUserEnv"
        msg_B "\t\t ${NewUserEnv}\n"

        acknowledge "Ready?"
        vim "$NewUserEnv"
    fi
}
