
# ---------------------------------------------------------------------

# source utilities
source "$JH_SRC/bash/include-utils.sh"

# custom error display
FailBadge='jh/common'
fail() {
    if [ -n "$FailBadge" ]; then
        echoerrx1 "[$FailBadge] $*"
    else
        echoerrx1 "$*"
    fi
}

# requirements
case $JH_OS in 
OSX)
    [[ $(iscmd gfind) ]] || fail "Sorry, this utility requires gfind on OSX (brew install findutils)."
    find() { gfind "$@"; }
esac

# ---------------------------------------------------------------------

# check existence, or create folder
#
#   ensureFolder Path
ensureFolder() {
    [ ! -d "$1" ] && mkdir -v "$1"
    [ ! -d "$1" ] && fail "Not a folder: $1"
}

# check that command is callable
# check that folder/file exists
#
#   checkDependency Name
checkDependency() { [[ $(iscmd "$1") ]] || fail "Missing dependency: $1"; }
checkFolder() { [ ! -d "$1" ] && fail "Folder not found: $1"; }
checkFile() { [ ! -f "$1" ] && [ ! -L "$1" ] && fail "File not found: $1"; }
checkFileOrCopy() { [ ! -f "$1" ] && [ ! -L "$1" ] && cp -iv "$2" "$1"; }

# check that folder is the root of a git repo
#
checkGitRoot() {
    checkFolder "$1"
    if [ ! -s "${1}/.git/HEAD" ] || [ ! -s "${1}/.git/config" ]; then 
        fail "Folder is not a git repo: $1"
    fi
}

# list folder contents
#
#   $1: name of folder to search (non-recursive)
#   $2: option -p to return full path
listFiles() {
    case $2 in 
    -p) find "$1" -maxdepth 1 -type f                   ;;
    *)  find "$1" -maxdepth 1 -type f -printf '%f\n'    ;;
    esac
}
listFolders() {
    case $2 in 
    -p) find "$1" -maxdepth 1 ! -path "$1" -type d                   ;;
    *)  find "$1" -maxdepth 1 ! -path "$1" -type d -printf '%f\n'    ;;
    esac
}

# find single folder or file, or die trying
#
#   findFolder Root Pattern
#   findFile Root Pattern
findFolder() {
    local f=$(find "$1" -maxdepth 1 -type d -name "$2")
    [ -z "$f" ] && fail "Folder not found: ${1}/$2"
    [ ! -d "$f" ] && fail "Not a folder (or multiple): $f"
    echo "$f"
}

findFile() {
    local f=$(find "$1" -maxdepth 1 -type f -name "$2")
    [ -z "$f" ] && fail "File not found: ${1}/$2"
    [ ! -f "$f" ] && [ ! -L "$f" ] && fail "Not a file (or multiple): $f"
    echo "$f"
}

# ---------------------------------------------------------------------

# TODO: to be used together with git option --shallow-since
nMonthsAgo() {
    local n=${1:-1}
    case $JH_OS in 
    OSX)    date -v-${n}m +"%Y-%m-%d"               ;;
    *)      date --date="$n month ago" +"%Y-%m-%d"     ;;
    esac
}

nYearsAgo() {
    local n=${1:-1}
    case $JH_OS in 
    OSX)    date -v-${n}y +"%Y-%m-%d"               ;;
    *)      date --date="$n year ago" +"%Y-%m-%d"      ;;
    esac
}

# ------------------------------------------------------------------------

# fix issues
source "$JH_SRC/jh/issues.sh"
