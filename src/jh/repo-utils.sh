
# ---------------------------------------------------------------------

# source utilities
source "${JH_SRC}/jh/common.sh"
FailBadge='repo-utils'

# check that there is a current user
[ -d "$JH_USR" ] || fail "User folder not found. Did you use jhusr yet?"

# paths
JH_REPO_SOURCE="${JH_USR}/repo"
JH_REPO_GROUP="${JH_VAR}/repo/group"
JH_REPO_CLONE="${JH_VAR}/repo/clone"
JH_REPO_SEP="--------------------------------------------------"

# ensure folder exists
ensureFolder "$JH_REPO_GROUP"
ensureFolder "$JH_REPO_CLONE"

# make sure exclude file is there
TemplateVCS="${JH_REPO_SOURCE}/tpl-vcs"
TemplateRepo="${JH_REPO_SOURCE}/tpl-repo"
checkFileOrCopy "$TemplateVCS" "${JH_TPL}/jh/vcs"
checkFileOrCopy "$TemplateRepo" "${JH_TPL}/jh/repo"

# ---------------------------------------------------------------------
# Filenames

jhr_vcsFile() { echo "${JH_REPO_SOURCE}/${1%.vcs}.vcs"; }
jhr_confFile() { echo "${JH_REPO_SOURCE}/${1%.conf}.conf"; }
jhr_listFile() { echo "${JH_REPO_SOURCE}/${1%.lst}.lst"; }
jhr_groupFile() { echo "${JH_REPO_GROUP}/${1#g:}"; }
jhr_cloneFolder() { echo "${JH_REPO_CLONE}/${1%.wiki}"; }
jhr_wikiFolder() { echo "${JH_REPO_CLONE}/${1%.wiki}.wiki"; }

jhr_isvcs() { [ -f "$( jhr_vcsFile "$1" )" ] && echo 1; }
jhr_isconf() { [ -f "$( jhr_confFile "$1" )" ] && echo 1; }
jhr_islist() { [ -f "$( jhr_listFile "$1" )" ] && echo 1; }
jhr_isgroup() { [ -f "$( jhr_groupFile "$1" )" ] && echo 1; }
jhr_isclone() { [ -d "$( jhr_cloneFolder "$1" )" ] && echo 1; }
jhr_iswiki() { [ -d "$( jhr_wikiFolder "$1" )" ] && echo 1; }

# sanitize group name with ${x#g:}
jhr_expandGroup() {
    case $1 in 
    g:*)  cat "$(jhr_groupFile "$1")" ;;
    *)    echo "$1" ;;
    esac
}

# check whether repo is a fork
jhr_isfork() {
    jhr_loadConf "$1"
    [ -n "$Fork" ] && echo 1
}

# ---------------------------------------------------------------------
# Loading

# uses information loaded from source in order to set
#   RepoURL      URL for the repo
#   WikiURL      URL for the wiki
#
# additionally sets the RemoteURL according to:
#   IsWiki=0    ->  RemoteURL=RepoURL
#   IsWiki=1    ->  RemoteURL=WikiURL
#
# if IsWiki is not set, then RemoteURL is unset.
#
# No input required. Uses ssh addresses by default.
# To use HTTPS instead, append :https to the source in the .lst file.
jhr_assignRemoteURLs() {
    case $RepoProto in 
    http*)
        Proto='https://'
        Psep='/'
        ;;
    *)
        Proto='git@'
        Psep=':'
        ;;
    esac
    case $Type in 
    github|gitlab)
        RepoURL="${Proto}${Host}${Psep}${User}/${RepoName}.git"
        WikiURL="${Proto}${Host}${Psep}${User}/${RepoName}.wiki.git"
        ;;
    bitbucket)
        RepoURL="${Proto}${Host}${Psep}${User}/${RepoName}.git"
        WikiURL="${Proto}${Host}${Psep}${User}/${RepoName}.git/wiki"
        ;;
    *)
        echo "Unknown (or missing) repo type: $Type"
        exit 1
    esac

    # set main remote URL
    case $IsWiki in 
    0) RemoteURL=$RepoURL ;;
    1) RemoteURL=$WikiURL ;;
    *) unset RemoteURL    ;;
    esac
}

# expect: host-user or filepath
#
# Load a .vcs file from the etc/ folder.
# The variables defined in that file should not conflict with the conf files.
#
#   VCSFile     Full path to .vcs file
#   Type        Repo type
#   Host        Remote address
#   User        Remote user
#   Remote      Name of the remote to give
#
# and assigns remote URLs (see below).
#
jhr_clearVCS() {
    unset Type 
    unset Host 
    unset User 
    unset Remote
}
jhr_loadVCS() {
    VCSFile=$(jhr_vcsFile "$1")
    checkFile "$VCSFile"
    jhr_clearVCS # cleanup scope
    source "$VCSFile"
}

# expect: repo or filepath
#
#   ConfFile    Full path to .conf file
#   Tag         For filtering mainly
#   Text        Short description
#   Branch      If set, consider VCS as upstream, and use this branch by default
#   Fork        If set, the URL of the original repo
#
jhr_clearConf() {
    unset Tag 
    unset Text 
    unset Branch 
    unset Fork
}
jhr_loadConf() {
    ConfFile=$(jhr_confFile "$1")
    checkFile "$ConfFile"
    jhr_clearConf # cleanup scope
    source "$ConfFile" 
    Repo=$(basename "$ConfFile")
    Repo=${Repo%.conf} # repo name = clone name
}

# expect: repo or filepath
#
#   ListFile    Full path to .lst file
#   SourceList  Array of sources (host-user, not remote names)
#
jhr_loadList() {
    ListFile=$(jhr_listFile "$1")
    checkFile "$ListFile"
    SourceList=$(cat "$ListFile")
    SourceList=( $SourceList )
    [ ${#SourceList[@]} -gt 0 ] || fail "Empty source-list."
}

# List files (.lst) contain lines of the form:
#   github-jhadida:repo:proto
#
# Parsing involves splitting around the ':'
#   VCS
#   RepoName
#   RepoProto
#
# Loading then involves calling loadVCS and assigning URLs.
jhr_clearSource() {
    unset VCS 
    unset RepoName 
    unset RepoProto
}
jhr_parseSource() {
    jhr_clearSource
    IFS=: read VCS RepoName RepoProto <<< "$1"
    if [ -z "$VCS" ] || [ -z "$RepoName" ]; then
        fail "Bad source format: $1"
    fi
    [ -z "$RepoProto" ] && RepoProto='ssh'
}
jhr_loadSource() {
    jhr_parseSource "$1"
    jhr_loadVCS "$VCS"
    jhr_assignRemoteURLs # set remote URLs
}

# assign variables for the path of the repo, accepting wiki prefixes
#
#   Repo
#   IsWiki
#   CloneName 
#   CloneFolder
#   WikiFolder
#
# then trigger loadConf, loadList and loadVCS (for Upstream).
#
jhr_loadRepo() {
    # deal with prefixes
    case $1 in
    w:*)
        IsWiki=1
        Repo=${1#w:}
        CloneName=${Repo}.wiki
        CloneFolder="${JH_REPO_CLONE}/$CloneName"
        ;;
    g:*)
        fail "This function does not accept group names: $1"
        ;;
    *)
        IsWiki=0
        Repo=$1
        CloneName=$Repo
        CloneFolder="${JH_REPO_CLONE}/$CloneName"
        ;;
    esac

    # load config
    jhr_loadConf "$Repo"
    jhr_loadList "$Repo"
    jhr_loadSource "${SourceList[0]}" # source[0] = upstream
}

jhr_listRepo() {
    find "$JH_REPO_SOURCE" -maxdepth 1 -type f -name '*.lst' -exec sh -c '
    for file do
        file=$(basename "$file")
        echo "${file%.lst}"
    done' sh {} +
}

jhr_checkRepo() {
    [ $# -gt 0 ] && jhr_loadRepo "$1"
    [ -z "$RemoteURL" ] && fail "No repo currently loaded."

    local RemoteList LocalRemotes
    local Remote_k Source_k Issue

    msg_b "$JH_REPO_SEP"
    msg_G "Checking $Repo:"
    Issue=0

    # Branch is set
    [ -z "$Branch" ] && { msg_R "Branch property should be set."; ((Issue++)); }

    # .vcs files exist for all sources
    for s in "${SourceList[@]}"; do
        jhr_parseSource "$s"
        [ $(jhr_isvcs "$VCS") ] || { msg_R "\tVCS not found: $s"; ((Issue++)); }
    done

    # check clone if it exists
    if  [ -d "$CloneFolder" ]; then 
    qpushd "$CloneFolder" # move to the clone

        # arrays of remotes and hosts
        LocalRemotes=$(git remote)
        RemoteList=()
        for s in "${SourceList[@]}"; do 
            jhr_loadSource "$s"
            RemoteList+=( "$Remote" )
        done

        # check remote config
        for k in ${!SourceList[@]}; do 
            # for each remote 
            Remote_k=${RemoteList[$k]}
            Source_k=${SourceList[$k]}

            # check that it is configured locally
            echo "$LocalRemotes" | grep -q "$Remote_k" 
            if [ $? -ne 0 ]; then
                msg_R "\tRemote not found locally: $Remote_k"
                ((Issue++))
                continue
            else
                jhr_loadSource "$Source_k"
                [[ "$RemoteURL" != "$(git remote get-url "$Remote_k")" ]] && { msg_R "\tWrong URL for remote: $Remote_k"; ((Issue++)); }
            fi
        done 

    qpopd
    fi

    case $Issue in 
    0) msg_G "\tok"                         ;;
    *) msg_M "\t$Issue issue(s) found."     ;;
    esac
}

# expect: group name or filepath
#
#   GroupName   Name without g: prefix
#   GroupFile   Full path to the group file in var/
#   Group       Array of repos (unchecked)
#
jhr_loadGroup() {
    Group=$(jhr_groupFile "$1")
    checkFile "$Group"
    GroupName=$(basename "$Group")
    GroupFile=$Group
    Group=$(cat "$Group")
    Group=( $Group )
}

# ---------------------------------------------------------------------
# tricky operations

# NOTE: the following only works for names WITHOUT SPACES

jhr_listRemove() {
    local out=()
    jhr_loadList "$1"
    shift 
    for s in "${SourceList[@]}"; do
        jhr_parseSource "$s"
        echo "$@" | grep -q "$VCS"

        # keep the source only if VCS is not found in input list
        [ $? -ne 0 ] && out+=( "$s" ) 
    done
    printf "%s\n" "${out[@]}" >| "$ListFile"
}

jhr_groupRemove() {
    jhr_loadGroup "$1"
    shift 
    for r in "$@"; do
        Group=( ${Group[@]/"$r"/} )
    done
    printf "%s\n" "${Group[@]}" >| "$GroupFile"
}

# ---------------------------------------------------------------------
# pretty display

jhr_dispRepo() {
    [ $# -lt 1 ] && fail "Requires repo name in input"
    Repo=${1#w:}
    count=0
    [[ $(jhr_isclone "$Repo") ]] && ((count++))
    [[ $(jhr_iswiki "$Repo") ]] && ((count++))
    case $count in 
    0)  echo "$Repo"                        ;;  # no clone nor wiki
    1)  echo -e "${ColG}${Repo}${FontD}"    ;;  # clone but no wiki
    2)  echo -e "${ColC}${Repo}${FontD}"    ;;  # clone and wiki
    esac
}

jhr_dispGroup() {
    [ $# -gt 0 ] && jhr_loadGroup "$1"
    [ -z "$GroupName" ] && fail "No group currently loaded."

    msg_b "$JH_REPO_SEP"
    msg_G "Group g:$GroupName"
    if (( ${#Group[@]} > 0 )); then
        echo  "    ${Group[@]}"
    else
        msg_W "    -empty-"
    fi
}

jhr_dispClone() {
    local R B

    [ $# -gt 0 ] && jhr_loadRepo "$1"
    [ -z "$CloneFolder" ] && fail "No repo currently loaded."

    qpushd "$CloneFolder"
    msg_b "$JH_REPO_SEP"
    case $IsWiki in 
    0)  msg_G "Repository $Repo"            ;;
    1)  msg_G "Repository $Repo (wiki)"     ;;
    esac
    B=($(git_lsbr))
    R=($(git remote))
    echo "          Path: $(pwd -P)"
    echo "    Branch(es): ${B[@]}"
    echo "     Remote(s): ${R[@]}"
    qpopd
}
