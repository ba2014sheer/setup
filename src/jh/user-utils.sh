
# ---------------------------------------------------------------------

# source utilities
source "${JH_SRC}/jh/common.sh"
FailBadge='user-utils'

JH_USER_STORE="${JH_VAR}/user"
JH_USER_FILE=".name"
JH_USER_ENV="env.sh"
JH_USER_FOLDERS=( repo host data bin app )

# requirements
case $JH_OS in 
OSX)
    [[ $(iscmd gdate) ]] || fail "Sorry, this utility requires gdate on OSX (brew install coreutils)."
    date() { gdate "$@"; }
esac
[[ $(iscmd wget) ]] || fail "Sorry, this utility requires wget to be installed."

# make sure root exists
ensureFolder "$JH_USER_STORE"

# ---------------------------------------------------------------------

# create a user folder
createUser() {
    [ $# -lt 1 ] && fail "Expects: User"
    local Folder="${JH_USER_STORE}/$1"

    [ -d "$Folder" ] && fail "User already exists: $Folder"
    ensureFolder "$Folder"
    
    echo "$1" >| "${Folder}/$JH_USER_FILE"
    for f in "${JH_USER_FOLDERS[@]}"; do 
        ensureFolder "${Folder}/$f"
    done

    cp -iv "${JH_TPL}/jh/env" "${Folder}/$JH_USER_ENV"
    msg_B "Please edit new user config: ${Folder}/$JH_USER_ENV"
}

# check is user folder exists
userExists() {
    [ $# -lt 1 ] && fail "Expects: User"
    [ -d "$JH_USER_STORE/$1" ] && echo 1
}

# change user silently
changeUser() {
    [ $# -lt 1 ] && fail "Expects: User"
    local Current=$(JH_getUserName)
    local User=$1

    [ -z "$Current" ] && fail "Empty username"
    [[ "$Current" == "$User" ]] && return 
    [[ $(userExists "$User") ]] || fail "User not found: $User"
    [[ $(userExists "$Current") ]] && fail "[bug] Duplicate user folder: $Current"

    \mv "$JH_USR" "$JH_USER_STORE/$Current"
    \mv "$JH_USER_STORE/$User" "$JH_USR"
}

# ------------------------------------------------------------------------

# # get the owner of a folder
# getUser() {
#     [ $# -lt 1 ] && fail "Expects: Folder"
#     local File="$1/$JH_USER_FILE"
#     [ ! -f "$File" ] && fail "File not found: $File"
#     cat "$File"
# }

# # name of current user
# currentUser() {
#     echo $(getUser "$JH_USR")
# }

# # check the owner of a folder
# userOwns() {
#     [ $# -lt 2 ] && fail "Expects: Folder Owner"
#     [[ $(getUser "$1") == "$2" ]] && echo 1
# }
