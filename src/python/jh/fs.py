
import os 
import math
import hashlib

from collections import namedtuple
from fnmatch import fnmatch
from os.path import normpath, realpath, basename, splitext
from os.path import join as pathjoin

from .color import *
from .util import *
from .stat import Size as FileSize
from .stat import Wrapper as FileStat

# ------------------------------------------------------------------------

class File:
    __slots__ = ('path', 'is_link','stat')

    def __init__(self, fpath, isLink=None, stat=None):
        self.path = normpath(fpath) 
        self.is_link = os.path.islink(fpath) if isLink is None else isLink
        self.stat = FileStat(fpath) if stat is None else FileStat(stat)
    
    @property 
    def name(self): return basename(self.path)
    @property
    def ext(self): return splitext(self.path)[1]
    @property 
    def is_hidden(self): return self.name.startswith('.')
    @property 
    def is_broken(self): return self.is_link and not os.path.exists(os.readlink(self.path))
    
    def realpath(self):
        return realpath(self.path) 
        
    def size(self):
        return self.stat.size[0]

    def checksum(self, **kwargs):
        kwargs.setdefault( 'init', self.name )
        checksum( self.path, **kwargs )

    def pretty_name(self):
        if self.is_broken:
            return '%s -> %s' % ( cfmt( self.name, fg='r', style='x' ), os.readlink(self.path) )
        elif self.is_link:
            return '%s -> %s' % ( cfmt( self.name, fg='c' ), self.realpath() )
        else:
            return self.name

# ------------------------------------------------------------------------

class Folder:
    __slots__ = ('path', 'dirs', 'files', 'depth', 'stat','is_link')

    def __init__(self, fpath, dirs=[], files=[], isLink=None, stat=None, depth=None):

        assert isinstance(dirs,list), TypeError('Bad type: dirs')
        assert isinstance(files,list), TypeError('Bad type: files')

        # dont check, to be compatible with ShallowFolder
        # assert all([ isinstance(x,Folder) for x in dirs ]), TypeError('Bad type: dirs')
        # assert all([ isinstance(x,File) for x in files ]), TypeError('Bad type: files')

        self.path = normpath(fpath) 
        self.dirs = dirs
        self.files = files 
        self.depth = depth
        self.stat = FileStat(fpath) if stat is None else FileStat(stat)
        self.is_link = os.path.islink(fpath) if isLink is None else isLink

    @property 
    def name(self): return basename(self.path)
    @property
    def ndirs(self): return len(self.dirs) 
    @property
    def nfiles(self): return len(self.files)
    @property
    def is_empty(self): return (self.nfiles + self.ndirs) == 0

    def realpath(self):
        return realpath(self.path) if self.is_link else self.path

    def size(self, recurse=False):
        s = sum([ x.size() for x in self.files ])
        if recurse:
            s += sum([ x.size(True) for x in self.dirs ])
        return s

    def total(self, recurse=False):
        t = self.ndirs + self.nfiles 
        if recurse:
            t += sum([ x.total(True) for x in self.dirs ])
        return t

    def pretty_name(self):
        if self.is_link:
            return '%s -> %s' % (
                cfmt( self.name, fg='m' ),
                cfmt( self.realpath(), fg='b', style='b' ),
            )
        else:
            return cfmt( self.name, fg='b', style='b' )

    def pretty_info(self, recurse=False):
        info = [ 
            'L%d' % self.depth,
            '%dd + %df' % (self.ndirs, self.nfiles), 
            'n=%d' % self.total(recurse),
            str(FileSize(self.size(recurse)))
        ]
        info = '(%s)' % ', '.join(info)
        return cfmt( info, fg='y' )

    def traverse(self, callback):
        """
        Depth-first traversal of folder tree.
        """
        callback( self )
        for d in self.dirs:
            d.traverse(callback)

# ----------  =====  ----------

class ShallowFolder(Folder):

    def __init__(self, *args, **kwargs):
        super().__init__( *args, **kwargs )

    def size(self, *args):
        return 0
    def total(self, *args):
        return 0

    def traverse(self, callback):
        callback(self)

    def pretty_name(self):
        if self.is_link:
            return '%s -> %s' % (
                cfmt( self.name, fg='m', bg='w' ),
                cfmt( self.realpath(), fg='b', style='b' ),
            )
        else:
            return cfmt( self.name, fg='b', bg='w', style='b' )

# ------------------------------------------------------------------------

_wrap_entry = namedtuple( 'DirEntryWrapper', 'name path isdir islink stat' )

def scantree( rootdir, followLinks=False, maxDepth=math.inf, exclude=[], relpath='' ):
    """
    Equivalent of os.walk, but using scandir and with more options.

    Folders can be excluded:
        - by depth
        - by relative name (unix-style)
        - if they are symlinks
    """
    visited = set()
    rootdir = normpath(rootdir)

    depth = relpath.count(os.sep) + (len(relpath) > 0)
    if depth >= maxDepth:
        return

    with os.scandir(rootdir) as it:
        for entry in it:
            if entry.is_file():
                relfile = pathjoin(relpath, entry.name)
                yield _wrap_entry( 
                    relfile, entry.path, 
                    False, entry.is_symlink(),
                    FileStat( entry.stat() ) )
            else:
                reldir = pathjoin(relpath, entry.name)
                absdir = realpath( entry.path )
                if entry.is_symlink() and not followLinks:
                    continue
                if any([ fnmatch(reldir,x) for x in exclude ]):
                    continue
                if absdir in visited:
                    continue 
                else:
                    visited.add(absdir)
                yield _wrap_entry(
                    reldir, entry.path, 
                    True, entry.is_symlink(), 
                    FileStat( entry.stat() ) )
                yield from scantree( entry.path, followLinks, maxDepth, exclude, reldir )

# ------------------------------------------------------------------------

def _find_match(name, patterns):
    for p in patterns:
        if fnmatch(name, p):
            return True 
    return False

def _entry_to_file(entry):
    try:
        return File( entry.path, entry.is_symlink(), entry.stat() )
    except:
        # handle broken symlink
        return File( entry.path, entry.is_symlink(), entry.stat(follow_symlinks=False) )

class Filter:
    def __init__(self, 
        maxDepth=math.inf, maxDirs=math.inf, maxFiles=math.inf, maxTotal=math.inf,
        exclude=[], ignore=[], shallow=[], followLinks=False ):
        
        self.maxDirs = maxDirs 
        self.maxFiles = maxFiles 
        self.maxTotal = maxTotal 
        self.maxDepth = maxDepth 
        self.exclude = exclude
        self.ignore = ignore
        self.shallow = shallow
        self.followLinks = followLinks

    def run(self, rootdir):
        self._visisted = set() # avoid loops
        return self._explore( rootdir, os.path.islink(rootdir), os.stat(rootdir), 0 )
    
    def _explore(self, fpath, isLink, stat, depth):

        # early cancelling for links
        if isLink:
            real = realpath(fpath)
            if not self.followLinks:
                return Folder( fpath, isLink=isLink, stat=stat, depth=depth )
            elif real in self._visisted:
                raise RuntimeError('Link previously visited: %s -> %s' % (fpath,real))
            else:
                self._visisted.add( real )

        # early cancelling for depth
        if depth > self.maxDepth or _find_match( basename(fpath), self.shallow ):
            return ShallowFolder( fpath, isLink=isLink, stat=stat, depth=depth )
        
        # list files and folders
        files = []
        dirs = []
        with os.scandir(fpath) as it:
            for entry in it:
                if entry.is_dir():
                    if not _find_match( entry.name, self.exclude ):
                        dirs.append(entry)
                else:
                    if not _find_match( entry.name, self.ignore ):
                        files.append(entry)

        # sort files and folders by name
        files = sorted( files, key=lambda x: x.name )
        dirs = sorted( dirs, key=lambda x: x.name )

        # apply limitations
        nfiles = len(files)
        ndirs = len(dirs)
        if ndirs > self.maxDirs \
        or nfiles > self.maxFiles \
        or (nfiles + ndirs) > self.maxTotal:
            return ShallowFolder( fpath, dirs, files, isLink, stat, depth )
        else:
            files = [ _entry_to_file(x) for x in files ]
            dirs = [ self._explore(x.path, x.is_symlink(), x.stat(), depth+1) for x in dirs ]
            return Folder( fpath, dirs, files, isLink, stat, depth )

# ----------  =====  ----------

def filter( rootdir, **kwargs ):
    F = Filter( **kwargs )
    return F.run( rootdir )
