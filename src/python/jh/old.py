
def _old_index( rootdir, recursive=True, followLinks=False, exclude=[], ignore='', digest='md5' ):
    """
    Build file "index" from specified root directory.
    Output is a dictionary with, for each file:
        hash: relpath

    where hash is a hex-string obtained by digesting files (default: md5),
    and relpath is a string with the relative path to the file.

    Subfolders can be excluded with their RELATIVE path in the array `exclude`.
    Filenames can be ignored using Unix-style file patterns with `ignore`.
    """

    assert os.path.isdir(rootdir), FileNotFoundError('Not found: %s' % rootdir)
    assert isinstance(exclude,list), 'Exclude should be a list'
    assert isinstance(ignore,str), 'Exclude should be a list'

    # process folder names
    out = {}
    visited = set()
    rootdir = normpath(rootdir)
    exclude = map( lambda x: normpath(x), exclude )

    for curdir, subdirs, files in os.walk(rootdir):

        # current folder
        absdir = realpath( curdir )
        reldir = relpath( curdir, rootdir )
        level = reldir.count(os.sep)

        # avoid loops
        if absdir in visited:
            continue 
        else:
            visited.add(absdir)

        # skip if excluded 
        if any([ reldir.startswith(x) for x in exclude ]):
            continue 

        # otherwise iterate files
        for filename in files:
            
            curfile = pathjoin( curdir, filename )
            relfile = pathjoin( reldir, filename )
            islink = os.path.islink(curfile)

            # ignore file
            if ignore and fnmatch(filename, ignore):
                continue
            # otherwise insert hash
            if not islink or followLinks:
                # cs = checksum( curfile, init=basename(curfile), algo=digest ) 
                cs = checksum( curfile, init=relfile, algo=digest ) 
                if cs in out:
                    raise KeyError( 'Hash conflict between "%s" and "%s"' % (relfile, out[cs]) )
                else:
                    out[cs] = relfile
        
        # break if non-recursive
        if not recursive:
            break

    return out
