
from .fs import *

# ------------------------------------------------------------------------

class Folders:

    def __init__(self, showFiles=False, showSizes=False, usrproc=None, recurse=False):
        # options
        self.showFiles = showFiles
        self.showSizes = showSizes # unused
        self.usrproc = usrproc
        self.recurse = recurse

    def show(self, rootdir, **kwargs):
        x = filter( rootdir, **kwargs )
        self._recurse( x, '', False, True )

    def _recurse(self, folder, padding, isLast, isFirst=False):
        
        pad = {
            'leg': '├── ',
            'end': '└── ',
            'wrn': '!   ',
            'tab': '   '
        }
        
        # print current folder
        tmp = padding[:-1]
        wrn = '' if isFirst else pad['wrn']
        txt =  folder.pretty_name() + ' ' + folder.pretty_info(self.recurse)
        
        leg = pad['end'] if isLast else pad['leg']
        leg = '' if isFirst else leg
        print( tmp + leg + txt )
        
        if self.usrproc:
            msg = self.usrproc(folder)
            if msg: print( tmp + wrn + msg )

        # stop if shallow folder
        if isinstance(folder, ShallowFolder):
            return

        # update padding
        if not isFirst:
            padding += pad['tab']

        # show folders first
        thenFiles = self.showFiles and (folder.nfiles > 0)
        for k,sub in enumerate(folder.dirs):
            isLast = (k+1 == folder.ndirs) and not thenFiles
            if isLast:
                self._recurse(sub, padding + ' ', isLast)
            else: 
                self._recurse(sub, padding + '|', isLast)

        # show files
        if self.showFiles:
            for k,file in enumerate(folder.files):
                isLast = k+1 == folder.nfiles 
                leg = pad['end'] if isLast else pad['leg']
                print( padding + leg + file.pretty_name() )
