
import os, stat, math
import pwd, grp
from datetime import datetime as dt

'''
Wrapper for os.stat_result, which contains all the information I usually need, 
in a form that is simple to interact with.

For example:
    x = jh.stat.File( '/path/to/file.zip' )

    # permissions
    if not x.perm['urw']:
        print 'Needs read+write permission!'

    # size
    if x.size['GB'] > 1:
        print 'File is too big'

    print(x.size) # auto-select correct unit

    # user/group info
    x.owner.name 
    x.group.id
    print(x.owner) # prints: name (id)

'''

# ------------------------------------------------------------------------

class Size:
    __slots__ = ('bytes', 'scale', 'units')

    def __init__(self, bsize):
        self.bytes = bsize 
        self.scale = math.log(max([ bsize, 1 ]))
        self.scale = int(self.scale // math.log(1024))
        self.units = ['B','kB','MB','GB','TB','PB']

    def __getitem__(self, key):
        try:
            if isinstance(key,str):
                s = self.units.index(key)
            else:
                s = int(key)
        except:
            raise ValueError('Unknown scale: {}'.format(key))

        return self.bytes / 1024**s

    def __str__(self):
        s = min([ len(self.units)-1, self.scale ]) 
        return '%.2f %s' % ( self[s], self.units[s] )

class Time:
    # access, modify, create/metadata
    __slots__ = ('a', 'm', 'c')

    def __init__(self, a,m,c):
        self.a = a
        self.m = m
        self.c = c

    def convert(self):
        if not isinstance( self.a, dt.date ):
            self.a = dt.fromtimestamp(self.a)
            self.m = dt.fromtimestamp(self.m)
            self.c = dt.fromtimestamp(self.c)

# ------------------------------------------------------------------------

class Owner:
    __slots__ = ('id', 'name')

    def __init__(self,uid):
        self.id = uid 
        self.name = pwd.getpwuid(uid).pw_name

    def __str__(self):
        return '%s (%d)' % (self.name, self.id)

class Group:
    __slots__ = ('id', 'name')

    def __init__(self, gid):
        self.id = gid 
        self.name = grp.getgrgid(gid).gr_name

    def __str__(self):
        return '%s (%d)' % (self.name, self.id)

class Perm:
    __slots__ = ('mode', 'octal', 'code')

    def __init__(self, mode):
        self.mode = mode 
        self.octal = oct(stat.S_IMODE(mode))
        self.code = {
            'u': {'r': stat.S_IRUSR, 'w': stat.S_IWUSR, 'x': stat.S_IXUSR, 's': stat.S_IXUSR | stat.S_ISUID},
            'g': {'r': stat.S_IRGRP, 'w': stat.S_IWGRP, 'x': stat.S_IXGRP, 's': stat.S_IXGRP | stat.S_ISGID},
            'o': {'r': stat.S_IROTH, 'w': stat.S_IWOTH, 'x': stat.S_IXOTH, 't': stat.S_IXOTH | stat.S_ISVTX},
        }

    def __getitem__(self, key):
        """
        Key should be: ur, ux, urw, grw
        I.e. first letter owner, rest permission
        """
        x = key[0]
        k = key[1:]
        for y in k:
            if not bool(self.mode & self.code[x][y]):
                return False 

        return True

    def __str__(self):
        return str(self.octal)

def perm2mask(p):
    """
    Turn permission string with 9 characters (e.g. "rwsr-x-wt") to a mask that can be used 
    with os.chmod(). The following should always be true:
        p == stat.filemode(perm2mask(p))[1:]
    """

    assert len(p) == 9, 'Bad permission length'
    assert all(p[k] in 'rw-' for k in [0,1,3,4,6,7]), 'Bad permission format (read-write)'
    assert all(p[k] in 'xs-' for k in [2,5]), 'Bad permission format (execute)'
    assert p[8] in 'xt-', 'Bad permission format (execute other)'

    m = 0

    if p[0] == 'r': m |= stat.S_IRUSR 
    if p[1] == 'w': m |= stat.S_IWUSR 
    if p[2] == 'x': m |= stat.S_IXUSR 
    if p[2] == 's': m |= stat.S_IXUSR | stat.S_ISUID 

    if p[3] == 'r': m |= stat.S_IRGRP 
    if p[4] == 'w': m |= stat.S_IWGRP 
    if p[5] == 'x': m |= stat.S_IXGRP 
    if p[5] == 's': m |= stat.S_IXGRP | stat.S_ISGID 

    if p[6] == 'r': m |= stat.S_IROTH 
    if p[7] == 'w': m |= stat.S_IWOTH 
    if p[8] == 'x': m |= stat.S_IXOTH 
    if p[8] == 't': m |= stat.S_IXOTH | stat.S_ISVTX

    return m

# ------------------------------------------------------------------------

class Wrapper:
    __slots__ = ('perm', 'size', 'owner', 'group', 'time')
    
    def __init__(self, fstat, convert=False):
        if isinstance(fstat,str):
            try:
                fstat = os.stat(fstat)
            except: 
                # handle broken symlinks
                fstat = os.lstat(fstat)

        # see also: 
        #   st_rsize on OSX
        #   st_birthtime on FreeBSD

        if isinstance(fstat, Wrapper):
            self = fstat
        else:
            self.size = Size( fstat.st_size ) 
            self.perm = Perm( fstat.st_mode )
            self.owner = Owner( fstat.st_uid )
            self.group = Group( fstat.st_gid )
            self.time = Time( fstat.st_atime, fstat.st_mtime, fstat.st_ctime )

        if convert:
            self.time.convert()
