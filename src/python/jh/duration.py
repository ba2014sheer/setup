
#==================================================
# @title        Duration representation
# @author       Jonathan Hadida
# @contact      Jhadida87 [at] gmail
#==================================================

class Duration:
    """
    Represent a time-duration as an 8-tuple:
        year, week, day, hour, minute, seconds, milli, micro

    Example:
    >>> Duration( 13*24*3600 + 4*3600 + 102*60 + 10.1234567 )
        (0.0, 1.0, 6.0, 5.0, 42.0, 10.0, 123.0, 456.0)
    """

    Factors = (
        365*24*3600, 7*24*3600, 24*3600,
        3600, 60, 1,
        1e-3, 1e-6
    )
    
    def __init__(self, sec=0):
        self._dur = (0,0,0,0,0,0,0,0)
        if sec > 0:
            self.from_seconds(sec)

    def from_seconds(self,sec):
        """
        Set from a duration in seconds.
        """

        assert sec > 0, 'Durations should be positive.'
        
        y, sec = divmod( sec, 365*24*3600 )
        w, sec = divmod( sec, 7*24*3600 )
        d, sec = divmod( sec, 24*3600 )
        h, sec = divmod( sec, 3600 )
        m, sec = divmod( sec, 60 )
        s, sec = divmod( sec, 1 )

        ms, sec = divmod( sec*1000, 1 )
        us, sec = divmod( sec*1000, 1 )
        
        self._dur = (y,w,d,h,m,s,ms,us)

    def to_seconds(self):
        return sum( a*b for a,b in zip( self._dur, Duration.Factors ) )

    def __repr__(self):
        return repr(self._dur)

    def __str__(self):    
        """
        Return a human-friendly string (suitable for display).
        """
        
        y,w,d,h,m,s,ms,us = self._dur # unpack
        if y > 0:
            return '%d years %d weeks %d days' % (y,w,d)
        elif w > 0:
            return '%d weeks %d days %dh' % (w,d,h)
        elif d > 0:
            return '%d days %dh %02dmin %02dsec' % (d,h,m,s)
        elif h > 0:
            return '%dh %02dmin %02dsec' % (h,m,s)
        elif m > 0:
            return '%dmin %02dsec' % (m,s)
        elif (s > 0 or ms > 0):
            return '%d.%03d sec' % (s,ms)
        elif us > 0:
            return '%d us' % (us)
        else:
            return '<null>'

# ----------  =====  ----------

def sec2str(sec):
    """
    Convert duration in seconds to a human-friendly string.

    Equivalent to str(dk.time.duration.Duration(sec))
    
    Related
    -------
    dk.time.duration.Duration
    """
    return str(Duration(sec))
