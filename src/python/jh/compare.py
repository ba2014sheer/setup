
import os

from fnmatch import fnmatch
from os.path import normpath, realpath, relpath, basename
from os.path import join as pathjoin

from .fs import scantree
from .util import checksum
from .color import cprint

# ------------------------------------------------------------------------

def index( rootdir, followLinks=False, exclude=[], ignore='', digest='md5', maxSize=10, allowDup=True ):
    """
    Build file "index" from specified root directory.
    Output is a dictionary with, for each file:
        hash: [relpath1, relpath2 ...]

    where hash is a hex-string obtained by digesting files (default: md5),
    and relpathX is a string with the relative path to the file.
    
    One hash can have several files if there are duplicates. Set allowDup=False
    to throw an error when duplicates are found. 

    Subfolders can be excluded with their RELATIVE path in the array `exclude`.
    Filenames can be ignored using Unix-style file patterns with `ignore`.

    Avoid digesting large files with maxSize (in MB); instead, register those
    files with basic stat information.
    """

    assert os.path.isdir(rootdir), FileNotFoundError('Not found: %s' % rootdir)
    assert isinstance(exclude,list), 'Exclude should be a list'
    assert isinstance(ignore,str), 'Exclude should be a list'

    # process folder names
    out = {}
    rootdir = normpath(rootdir)

    for entry in scantree(rootdir, followLinks=followLinks, exclude=exclude):

        relfile = entry.name
        filename = basename(relfile)

        # skip criteria
        if entry.isdir:
            continue
        if ignore and fnmatch(filename, ignore):
            continue

        # compute index-key
        cs = checksum( entry.path, algo=digest, maxSize=maxSize*8e6 ) 

        # if entry.stat.size['MB'] > maxSize:
        #     cs = relfile + '+' + str(entry.stat.size['B'])
        #     cs = checksum( entry.path, init=relfile, algo=digest ) 

        # register file
        if cs in out:
            if not allowDup:
                raise KeyError( 'Hash conflict between "%s" and "%s"' % (relfile, out[cs]) )
            else:
                out[cs].append(relfile)
        else:
            out[cs] = [relfile]

    return out

# ------------------------------------------------------------------------

def _showdiff( a, b, col, sep ):
    """
    Find keys in a which are not in b.
    For such keys, display first element (index 0) of list-value, and
    display remaining elements (duplicate files) in yellow.
    """
    for k,v in a.items():
        if k not in b:
            cprint( sep + v[0], fg=col )
            for dup in v[1:]:
                cprint( "\t" + dup, fg='y' )

def compare( ref, tgt, showMissing=True, showAdded=False, excludeRef=[], excludeTgt=[], **kwargs ):
    """
    Index two folders, and compare their contents.

    First folder is taken as references, so:
     - "missing" = in A, but not in B;
     - "added"   = in B, but not in A.
    """

    cprint( 'Indexing reference folder "%s"...' % (ref) )
    ref = index(ref, exclude=excludeRef, **kwargs)

    cprint( 'Indexing target folder "%s"...' % (tgt) )
    tgt = index(tgt, exclude=excludeTgt, **kwargs)

    cprint( '--------------------------------------------------', style='b' )

    if showMissing:
        _showdiff( ref, tgt, 'r', '- ' )
    if showAdded:
        _showdiff( tgt, ref, 'g', '+ ' )

# ------------------------------------------------------------------------

def showdup( folder, **kwargs ):
    """
    Index folder, and find duplicate files.
    """
    kwargs["allowDup"] = True
    idx = index( folder, **kwargs )

    for k,v in idx.items():
        if len(v) > 1:
            cprint( v[0], fg='c' )
            for dup in v[1:]:
                cprint( "\t" + dup, fg='r' )
