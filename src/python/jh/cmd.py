
import os 
import shutil 
import subprocess 
import logging
from functools import partial

# ------------------------------------------------------------------------

def runcmd( *args, **kwargs ):
    """
    Run command by concatenating all positional args.

    Keywords are interpreted as options of subprocess.run, with defaults:
        encoding  utf-8
             env  os.environ
          stdout  subprocess.PIPE
          stderr  subprocess.PIPE
           check  True

    Output is an instance of subprocess.CompletedProcess
    See: https://docs.python.org/3/library/subprocess.html#subprocess.CompletedProcess
    """

    # set default options
    kwargs.setdefault( 'encoding', 'utf-8' )
    kwargs.setdefault( 'env', os.environ )
    kwargs.setdefault( 'stdout', subprocess.PIPE )
    kwargs.setdefault( 'stderr', subprocess.PIPE )
    kwargs.setdefault( 'check', True )

    # assemble command
    if not all([ isinstance(x,str) for x in args ]):
        raise TypeError( 'Command parts should be strings.' )

    return subprocess.run( args, **kwargs )

# ------------------------------------------------------------------------

class CommandWrapper:
    
    def __init__(self, cmd, logHandler=logging.StreamHandler(), logLevel=logging.INFO, logName='jh', **kwargs):
        """
        Wrap calls to subprocess.run, with default options:
            encoding: utf-8
                 env: os.environ
               check: True
              stdout: PIPE
              stderr: PIPE

        Logging can also be customised with options:
            logHandler: logging.StreamHandler()
              logLevel: logging.INFO
               logName: jh

        TODO: EXAMPLE USAGE
        """

        # check command path
        if not isinstance(cmd,str):
            raise TypeError('Command should be a string')
        if not os.path.isfile(cmd):
            cmd = shutil.which(cmd)
        if not os.access( cmd, os.X_OK ):
            raise PermissionError('Command not found, or insufficient permissions')

        # configure logging
        self.log = logging.getLogger( logName )
        self.log.addHandler( logHandler )
        self.log.setLevel( logLevel )

        # assign members
        self.proc = kwargs
        self._cmd = cmd

    def __call__(self, *args):
        """
        Run command by concatenating all inputs.

        Args and values should be strings (unicode supported).
        Output is a subprocess.CompletedProcess
        """

        # run command
        proc = runcmd( self._cmd, *args, **self.proc )

        # log results
        self.log.debug( u'%s' % proc.stdout )
        if proc.returncode:
            self.log.error( u'%s' % proc.stderr )

        return proc

    def __getattr__(self,name):
        """
        Create a handler for a particular subcommand.

        For example:
            git = CommandWrapper('git')
            git.commit( '-am', 'My message' )
        """
        return partial( self, name )
