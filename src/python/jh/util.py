
import os 
import tarfile
import hashlib 

from os.path import normpath, dirname, basename, isdir
from os.path import join as pathjoin

# ------------------------------------------------------------------------

def maketar( folder, compress='' ):
    """
    Create tar archive for input folder.
    Optionally compress:
        gz      gzip
        bz2     bzip2
        xz      lzma
    """
    if compress:
        mode = 'w:' + compress
        ext = '.tar.' + compress
    else:
        mode = 'w'
        ext = '.tar'
    
    folder = normpath(folder)
    parent = dirname(folder)
    name = basename(folder)
    tarname = name + ext
    tarpath = pathjoin(parent, tarname)

    assert isdir(folder), FileNotFoundError('Not a folder: %s' % folder)
    with tarfile.open( tarpath, mode ) as tar:
        tar.add( folder, arcname=name )

# ------------------------------------------------------------------------

def checksum( fpath, algo='md5', init=None, blockSize=65536, maxSize=80e6 ):
    """
    Read file and digest contents as hash, using specified algorithm (default: sha256).
    If the file is larger than maxSize (in bits), then only consider blocks until the 
    maxSize for the hash. By default, maxSize=10MB (80e6 bits).
    
    The output is a hex-string corresponding to the digest.

    For a list of algorithms: https://docs.python.org/3/library/hashlib.html
    """
    algo = getattr(hashlib, algo)()
    if init:
        algo.update( str(init).encode('utf-8') )

    size = 0
    with open(fpath, 'rb') as f:
        for block in iter(lambda: f.read(blockSize), b''):
            algo.update(block)
            size += blockSize
            if size >= maxSize:
                break

    return algo.hexdigest()
