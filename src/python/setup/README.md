
Re-write utilities for repo/data management with Python.

Repos, vcs, groups and data sources can be stored using JSON files (see templates for format).
For data-sync, use PouchDB and watchdog to register all filesystem operations.
