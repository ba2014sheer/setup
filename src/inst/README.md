
# Installation scripts

All scripts in this folder install stuff on the local machine.

Requirements are checked at the beginning of the script, and may abort the install.
Then, check if stuff is already installed, and if it is, don't reinstall it.

Finally, all scripts named `pkg-*.sh` install packages using other tools that need to be installed (e.g. `brew`, `conda`, etc).
