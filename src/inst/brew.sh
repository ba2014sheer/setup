#!/bin/bash

source "${JH_SRC}/bash/include-utils.sh"

# abort if brew is already on the path
[[ $(iscmd "brew") ]] && echoerrx1 "brew is already on your path: $(which brew)"

case $JH_OS in
Linux) 
    echoerrx1 "There is no Homebrew on Linux..."
    ;;
OSX)
    # see: https://brew.sh/
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    ;;
*)
    echoerrx1 "Unknown system: $JH_OS"
esac
