#!/bin/bash

# source utils
source "${JH_SRC}/bash/include-utils.sh"

# [[ $(nocmd yarn) ]] && echoerrx1 "yarn is needed to install packages"

# base packages
Packages=(
    eslint jslint typescript
    http-server nodemon pm2 webpack
    couchdb pouchdb
)

# OS-specific packages
case $JH_OS in 
OSX)
    Packages+=(ttab)
esac

# run install
# yarn global add "${Packages[@]}"
npm i -g "${Packages[@]}"
