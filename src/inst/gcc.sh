#!/bin/bash

# source utils
source "${JH_SRC}/bash/include-utils.sh"

usage() {
[ $# -gt 0 ] && echoerr "$*"
cat <<EOF

Download and compile a particular version of GCC.

    jhinst gcc <Version> <Mirror> <Build> <Prefix>

For available versions, visit:  https://gcc.gnu.org/releases.html
For available mirrors, visit:   https://gcc.gnu.org/mirrors.html

The Build folder is typically "${HOME}/.local/src".
The Prefix folder is typically "${HOME}/.local/bin/gcc"
Whatever you choose, if these do not exist, they will be created.

See also this post for instructions with apt-get:
    https://unix.stackexchange.com/a/410863/45354

EOF
exit 0
}

# extract inputs
[ $# -lt 4 ] && usage 
Version=$1
Mirror=$(rtrim_slash "$2")
Build=$(rtrim_slash "$3")
Prefix=$(rtrim_slash "$4")

# create folders
[ ! -d "$Build" ] && mkdir -pv "$Build"
[ ! -d "$Build" ] && echoerrx1 "Could not create folder: $Build"
[ ! -w "$Build" ] && echoerrx1 "Directory not writable: $Build"

[ ! -d "$Prefix" ] && mkdir -pv "$Prefix"
[ ! -d "$Prefix" ] && echoerrx1 "Could not create folder: $Prefix"
[ ! -w "$Prefix" ] && echoerrx1 "Directory not writable: $Prefix"

qpushd "$Build"
Build=$(pwd -P)

# download version
Name=gcc-${Version}
File=${Name}.tar.gz 
wget "${Mirror}/releases/${Name}/$File"
[ -f "$File" ] || echoerrx1 "Could not download: $File"

# uncompress and move to extracted folder
tar xzf $File

Folder="${Build}/$Name"
[ ! -d "$Folder" ] && echoerrx1 "Couldn't find the extracted folder: $Folder"
qpushd "$Folder"

# download dependencies
./contrib/download_prerequisites

# run configuration
ObjFolder=${Folder}/objdir
mkdir "$ObjFolder"
[ ! -d "$Folder" ] && echoerrx1 "Couldn't create object folder: $ObjFolder"
./configure --prefix="$Prefix" --enable-languages=c,c++,fortran,go --disable-multilib
[ $? -ne 0 ] && echoerrx1 "Issues during configuration."

# display message about what to do next
msg_bG "Dowloaded and configured gcc version $Version for installation in: $Prefix"
msg_G  "To proceed, run:"
msg_G  "    cd '$Folder'"
msg_G  "    make -j 8 && make install"
