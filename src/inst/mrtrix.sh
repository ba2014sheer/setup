#!/bin/bash

# source utils
source "${JH_SRC}/bash/include-utils.sh"

# documentation
usage() {
[ $# -gt 0 ] && echoerr "$@"
cat <<EOF 

Install latest version of MRtrix.

    jhinst mrtrix <Folder>

Download sources and dependencies from GitHub + BitBucket.
For more info, see:
    https://mrtrix.readthedocs.io/en/latest/installation/linux_install.html

For dependencies:
    Eigen: 
        http://eigen.tuxfamily.org/, 
        https://bitbucket.org/eigen/eigen/src/default/
    Qt: 
        https://www.qt.io/download-qt-installer
        
EOF
exit 0
}

# parse inputs
[ $# -lt 1 ] && usage
Folder=${1%/}
qpushd "$Folder"

# download Eigen
EigenLink=http://bitbucket.org/eigen/eigen/get/3.3.5.tar.bz2
EigenFile=eigen.tar.bz2
if [ ! -d "eigen" ]; then
    
    # msg_bG "Cloning Eigen..."
    # hg clone https://jhadida@bitbucket.org/eigen/eigen
    # DO NOT clone master: https://github.com/MRtrix3/mrtrix3/issues/1464

    msg_bG "Downloading Eigen..."
    wget $EigenLink -O $EigenFile || echoerrx1 "Failed to download Eigen"
    mkdir eigen && tar -xf $EigenFile -C eigen --strip-components=1 && mv $EigenFile eigen/
    [ $? -gt 0 ] && echoerrx1 "Error extracting Eigen."

else
    msg_bC "Found Eigen library in: $Folder/eigen"
fi
EigenPath=$(cd eigen && pwd -P)

# download Qt: not necessary 
# http://mirrors.ukfast.co.uk/sites/qt.io/archive/online_installers/3.0/qt-unified-mac-x64-3.0.5-online.dmg
# http://mirrors.ukfast.co.uk/sites/qt.io/archive/online_installers/3.0/qt-unified-linux-x64-3.0.5-online.run

# download and build
if [ ! -d "mrtrix3" ]; then
    msg_bG "Cloning MRtrix..."
    git clone https://github.com/MRtrix3/mrtrix3.git
else
    msg_bC "Found MRtrix repo in: $Folder/mrtrix3"
fi

msg_bG "Configuring MRtrix..."
cd mrtrix3
EIGEN_CFLAGS="-isystem '$EigenPath'" ./configure -nogui
if [ $? -gt 0 ]; then 
    echoerr "Error configuring MRtrix. See config log below:"
    cat configure.log
fi

msg_bG "Building MRtrix..."
./build
[ $? -gt 0 ] && echoerrx1 "Error building MRtrix."

# display message
msg_b "Installation complete! :)"
qpopd
