#!/bin/bash

# source utils
source "${JH_SRC}/bash/include-utils.sh"
[[ $(nocmd wget) ]] && echoerrx1 "This utility requires wget"

usage() {
[ $# -gt 0 ] && echoerr "$@"
cat <<EOF

Manual FUSE installation.

    jhinst fuse <Version> <Target>

See for versions: https://github.com/libfuse/libfuse/releases

EOF
exit 0
}

# download
# extract
# configure
# make
# install