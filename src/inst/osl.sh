#!/bin/bash

# source utils
source "${JH_SRC}/bash/include-utils.sh"

# documentation
usage() {
[ $# -gt 0 ] && echoerr "$@"
cat <<EOF 

Install latest version of OSL.

    jhinst osl <Folder>

Download package from Wooly's folder, and run script to convert subfolders to repos.

EOF
exit 0
}

# parse inputs
[ $# -lt 1 ] && usage
Folder=$1

# move there
[ ! -d "$Folder" ] && echoerrx1 "Folder not found: $Folder"
qpushd "$Folder"
Folder=$(pwd -P)

# download and extract
wget http://users.fmrib.ox.ac.uk/~woolrich/osl/osl.tar.gz
tar xzvf osl.tar.gz 

# convert folders to repos
OSL_Folder="${Folder}/osl/osl-core"
[ ! -d "$OSL_Folder" ] && echoerrx1 "Folder not found: $OSL_Folder"
cd "$OSL_Folder"
bash convert_to_git.sh

# recompile SPM
SPM_Folder="${Folder}/osl/spm12/src"
[ ! -d "$SPM_Folder" ] && echoerrx1 "Folder not found: $SPM_Folder"
cd "$SPM_Folder"

make distclean
make external-distclean
make && make install
make external && make external-install

# conclusion
qpopd
echo "-----------------------------------------------"
echo "-----------------------------------------------"
echo "                  D O N E"
echo ""
echo "Now boot Matlab and type:"
echo ""
echo "  addpath('${OSL_Folder}');"
echo "  osl_startup();"
echo "  osl_recompile_fieldtrip();"
echo "  osl_check_installation();"
echo ""
