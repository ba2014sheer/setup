#!/bin/bash

# source utils
source "${JH_SRC}/bash/include-utils.sh"

# customise the list of packages
Additional=()
case $1 in 
--all)
    Additional+=(
        pandoc youtube-dl
    )
    ;;
esac

# install packages
case $JH_OS in 
OSX)
    brew tap caskroom/cask
    brew cask install gimp inkscape 
    brew install imagemagick ffmpeg 

    (( ${#Additional[@]} > 0 )) && brew install "${Additional[@]}"
    ;;
Linux)
    # +agave
    Distro=$(linux_distro)
    case $Distro in 
    ubuntu | debian)
        echoerrx1 "Sorry, can't install on Debian/Ubuntu for now."
        ;;
    redhad | centos)
        echoerrx1 "Sorry, can't install on RedHat/CentOS for now."
        ;;
    *)
        echoerrx1 "Unknown distribution: $Distro"
    esac
    ;;
esac

# show links for further installation
msg_bG "Also see the following links:"
msg_C  "    VLC     https://www.videolan.org/vlc/"
