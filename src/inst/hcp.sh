#!/bin/bash

# source utilities
source "${JH_SRC}/bash/include-utils.sh"
[[ $(nocmd wget) ]] && echoerrx1 "This utility requires wget"

usage() {
[ $# -gt 0 ] && echoerr "$@"
cat <<EOF

Manual installtion of HCP tools.

    jhinst hcp <Folder>

This utility downloads and extracts Caret and Connectome Workbench to the desired folder.
A typical folder would be:
    ${HOME}/.local/src

If the specified folder does not exist, then it is created.

EOF
exit 1
}

# extract inputs
[ $# -lt 1 ] && usage 
Folder=$(rtrim_slash "$1")

# create target folder
[ ! -d "$Folder" ] && mkdir -pv "$Folder"
[ ! -d "$Folder" ] && echoerrx1 "Could not create folder: $Folder"
qpushd "$Folder"



# Caret
Version="v5.65"
msg_C "\nCaret ${Version}"
msg_C "Check for newer versions at: http://brainvis.wustl.edu/wiki/index.php/Caret:Download"

# platform-dependent archive
case $JH_OS in 
OSX)    File=caret_distribution_Mac64.${Version}.zip    ;;
Linux)  File=caret_distribution_Linux64.${Version}.zip  ;;
esac

if [ -f "$File" ]; then
    msg_C "File already exists: $Folder/$File\n"
else

    # download caret
    msg_C "\tDownloading..."
    wget --user Rabbit --password Carrot "http://brainmap.wustl.edu/pub/caret/$File"
    [ ! -f "$File" ] && echoerrx1 "Downloaded file not found: ${Folder}/$File"

    # if caret dir already exists, back it up
    [ -d caret ] && mv -nv caret "caret-$(timestamp)"
    [ -d caret ] && echoerrx1 "Could not backup folder: $Folder/caret"

    # extract archive
    msg_C "\tExtracting..."
    unzip "$File"
    msg_C "Done!\n"

fi



# Workbench
Version="v1.3.2"
msg_C "\nConnectome Workbench ${Version}"
msg_C "Check for newer versions at: https://www.humanconnectome.org/software/get-connectome-workbench"

# platform-dependent archive
case $JH_OS in 
OSX)
    File=workbench-mac64-${Version}.zip
    ;;
Linux)
    # weak test to distinguish Debian/RedHat
    if [[ $(iscmd yum) ]]; then 
        File=workbench-rh_linux64-${Version}.zip
    else 
        File=workbench-linux64-${Version}.zip
    fi
    ;;
esac

if [ -f "$File" ]; then 
    msg_C "File already exists: $Folder/$File\n"
else

    # download workbench
    msg_C "\tDownloading..."
    wget "https://ftp.humanconnectome.org/workbench/$File"
    [ ! -f "$File" ] && echoerrx1 "Downloaded file not found: ${Folder}/$File"

    # if workbench dir already exists, back it up
    [ -d workbench ] && mv -nv workbench "workbench-$(timestamp)"
    [ -d workbench ] && echoerrx1 "Could not backup folder: $Folder/workbench"

    # make sure the target file is there, and extract it
    msg_C "\tExtracting..."
    unzip "$File"
    msg_C "Done!\n"

fi

# conclusion
msg_bG "Dowloaded Caret and Workbench in folder: $Folder"
msg_G  "There should be subfolders named like 'bin_macosx64'; find them and add them to your path (e.g. in your bash_profile)."
