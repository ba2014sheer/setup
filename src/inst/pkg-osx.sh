#!/bin/bash

usage() {
cat <<EOF
    jhinst pkg-osx --basic     (default)
    jhinst pkg-osx --all
EOF
exit 0
}

# get inputs
[ $# -lt 1 ] && usage 
Level=$1

# source utils
source "${JH_SRC}/bash/include-utils.sh"

# check requirements
[[ $JH_OS != OSX ]] && echoerrx1 "System is not OSX!"
[[ $(nocmd brew) ]] && echoerrx1 "Could not find Homebrew, run: jhinst brew"

# list packages
case $Level in 
--basic)
    Pkg=( coreutils findutils diffutils rsync wget curl ) ;;
--all)
    Pkg=(
        coreutils findutils diffutils rsync wget curl
        openssl netcat ssh-copy-id
        vim emacs baobab
        zip gzip bzip2 p7zip unrar
    ) ;;
*)
    echoerrx1 "Unknown option: $Level" ;;
esac

# install them
brew update
brew install "${Pkg[@]}"

# show links for further installation
msg_bG "Also see the following:"
msg_C  "    OSX FUSE/SSHFS          https://osxfuse.github.io/"
msg_C  "    Karabiner-elements      https://pqrs.org/osx/karabiner/"
msg_C  "    Scroll reverser         https://pilotmoon.com/scrollreverser/"
msg_C  "    Skim PDF                https://skim-app.sourceforge.io/"
