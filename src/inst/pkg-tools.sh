# Essentials: 
#   vim, emacs, git, rsync, wget, curl, 
#   coreutils, findutils
#   openssl, gnupg
#   colordiff, tree, ssh-copy-id
#
# Filesystem:
#   ntfs-3g, baobab
#   testdisk, hardinfo
#   s3fs
#
# Archive: 
#   zip, gzip, bzip2, p7zip, unrar
#