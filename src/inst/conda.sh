#!/bin/bash

# see: https://conda.io/docs/user-guide/install/index.html

usage() {
cat <<EOF
    jhinst conda  --show-versions                 => open browser to show anaconda versions
    jhinst conda  PythonVersion                   => miniconda
    jhinst conda  PythonVersion  AnacondaVersion  => anaconda

    Python version is 2 or 3.
    Anaconda version can be found at:
        https://www.anaconda.com/download/
        https://repo.anaconda.com/archive/
EOF
exit 0
}

# validate inputs
[ $# -lt 1 ] && usage
PythonVersion=$1
AnacondaVersion=$2

# source utils
source "${JH_SRC}/bash/include.sh"

# abort if conda is already on the path
if [[ $(iscmd "conda") ]]; then
    [[ $(confirm "${ColR}conda already exists: $(which conda)${FontD}
    Continue anyway?") ]] || exit 0
fi

# check requirements
Req=( wget python )
for r in "${Req[@]}"; do
    [[ $(iscmd "$r") ]] || echoerrx1 "$r is required."
done

case $PythonVersion in
2|3)  ;;
--show-versions)     
    open "https://www.anaconda.com/download/"; usage    ;;
*)  echoerrx1 "Invalid python version: $PythonVersion"  ;;
esac

if [ -z "$AnacondaVersion" ]; then
    InstallType='mini'
else 
    InstallType='ana'
fi

# useful variables
case $InstallType in 
mini)
    case $JH_OS in 
    OSX)    DL_URL="https://repo.continuum.io/miniconda/Miniconda${PythonVersion}-latest-MacOSX-x86_64.sh"  ;;
    Linux)  DL_URL="https://repo.continuum.io/miniconda/Miniconda${PythonVersion}-latest-Linux-x86_64.sh"   ;;
    esac
    ;;
ana)
    case $JH_OS in 
    OSX)    DL_URL="https://repo.anaconda.com/archive/Anaconda${PythonVersion}-${AnacondaVersion}-MacOSX-x86_64.sh"  ;;
    Linux)  DL_URL="https://repo.anaconda.com/archive/Anaconda${PythonVersion}-${AnacondaVersion}-Linux-x86_64.sh"   ;;
    esac

    # test version
    [[ $(url_reachable "$DL_URL") ]] || echoerrx1 "Invalid anaconda version: $AnacondaVersion"
    ;;
esac

# download to temporary folder
ExecName=$(basename "$DL_URL")
msg_bC "Downloading to /tmp: $DL_URL"
wget "$DL_URL" -P "/tmp"

msg_bC "Executing /tmp/${ExecName}:"
bash "/tmp/$ExecName"

# notify
msg_bG '----------------------------------------'
msg_G  'Done. Now add the following to ~/.bash_profile.local:\n'
msg_C  'CondaRoot="/path/to/anaconda3"'
msg_C  'CondaProfile="$CondaRoot/etc/profile.d/conda.sh"'
msg_C  'if [ -d "$CondaRoot" ]; then'
msg_C  '    path_prepend "$CondaRoot/bin"'
msg_C  '    source "$CondaProfile"'
msg_C  '    #conda activate yourenv'
msg_C  'fi\n'
msg_G  'You can create a new environment with:\n'
msg_C  '    conda create -n py36 python=3.6 anaconda\n'
