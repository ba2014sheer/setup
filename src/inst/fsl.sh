#!/bin/bash

# source utils
source "${JH_SRC}/bash/include-utils.sh"

usage() {
[ $# -gt 0 ] && echoerr "$@"
cat <<EOF

Download and run FSL installer.

    jhinst fsl
    
EOF
exit 0
}

msg_bG "Installing FSL..."
msg_G  "    more info at: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation"
msg_G  "     register at: https://fsl.fmrib.ox.ac.uk/fsldownloads_registration\n"

qpushd "$JH_TMP"
wget https://fsl.fmrib.ox.ac.uk/fsldownloads/fslinstaller.py
[ ! -f "fslinstaller.py" ] && echoerrx1 "Something went wrong during the download.\nFile not found: $(pwd)/fslinstaller.py"
python2 ./fslinstaller.py

# Instructions:
#   https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation
#   https://fsl.fmrib.ox.ac.uk/fsldownloads_registration

# Sources at:
# https://fsl.fmrib.ox.ac.uk/fsldownloads/fsl-5.0.11-sources.tar.gz
