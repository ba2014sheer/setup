#!/bin/bash

# source utils
source "${JH_SRC}/bash/include-utils.sh"
[[ $(nocmd wget) ]] && echoerrx1 "wget needed to download NVM"

Target=${1:-"$HOME/.nvm"}

if [[ $(nocmd nvm) ]]; then

    VER='v0.33.11'
    TMP=$(mktemp) # prevent auto-append to profile, redirect to tempfile instead

    # make sure temp file exists
    # TODO: apparently mktemp requires a template on some OSX versions???
    [ ! -f "$TMP" ] && echoerrx1 "Could not create temporary file: $TMP"

    # make sure target folder exists
    [ ! -d "$Target" ] && mkdir "$Target"
    [ ! -d "$Target" ] && echoerrx1 "Could not create folder: $Target"

    # download install script and run
    msg_b "Installing NVM version (Aug 2018): $VER"
    msg_b "Check for newer versions at: https://github.com/creationix/nvm#install-script"
    wget -qO- "https://raw.githubusercontent.com/creationix/nvm/${VER}/install.sh" | PROFILE="$TMP" NVM_DIR="$Target" bash

    # display info about 
    msg_G "\n--------------------------------------------------"
    msg_G "Please paste the following lines in your profile:\n"
    cat "$TMP"
    msg_G "\n--------------------------------------------------\n"

    # source it to be able to use nvm right away
    source "$TMP"

fi

nvm install node

msg_bG '--------------------------------------------------\n'
msg_G  'Done. Now use the installed version with:\n'
msg_C  '    nvm use stable\n'
msg_G  'If that spits out an error like'
msg_G  '    "nvm is not compatible with the npm config ..."'
msg_G  'see this issue: https://stackoverflow.com/a/47861348/472610\n'
msg_G  'Otherwise, if all is good, run:'
msg_C  '    npm i -g yarn'
msg_C  '    jhinst pkg-node\n'
msg_G  'Finally, to add the global packages to the path, add this to your profile:'
cat <<EOF
${ColC}
# Yarn path
if [[ \$(iscmd yarn) ]]; then
    export YARN_GLOBAL=\$(yarn global dir)
    path_prepend "\$YARN_GLOBAL/node_modules/.bin"
fi 
${FontD}
EOF
