
# the base URL and the archive file to download, e.g.:
#   URL=https://gitlab.com/project/releases
#   Archive=name-v1.0.tar.gz
VERSION="1.0.18"
URL="https://www.colordiff.org"
Archive="colordiff-${VERSION}.tar.gz"

# basic configure command -- make this as complicated as needed
ConfigCmd() {
    ./configure --prefix="$Prefix"
}

# display info after install (e.g. link to checkout new versions)
MoreInfo() {
    msg_B "Checkout newer versions (currently $VERSION) at: $URL"
}
