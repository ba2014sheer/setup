#!/bin/bash

source "$JH_ROOT/inc/scriptutils.sh"
reqcmd "wget"

Folder="${HOME}/.local/src"
makedir "$Folder"

Version=1.7.0
echo "Tree v$Version"
echo "Find newer versions at: http://mama.indstate.edu/users/ice/tree/"

Name="tree-$Version"
File="${Name}.tgz"

wget "http://mama.indstate.edu/users/ice/tree/src/$File" || echoerrx1 "Download issue"
tar xzf "$File" || echoerrx1 "Extraction issue: $File"
chkdir "$Name"

msg_bG '-----------------------------------------------'
msg_G  'Done. Now edit the Makefile:\n'
msg_C  "    vim $(pwd)/Makefile\n"
msg_G  'Uncomment suitable options and set prefix to:\n'
msg_C  "    ${HOME}/.local\n"
msg_G  'Then make && make install.\n'
