#!/bin/bash

# source utilities
source "${JH_SRC}/bash/include-utils.sh"
[[ $(nocmd wget) ]] && echoerrx1 "This utility requires wget"

usage() {
[ $# -gt 0 ] && echoerr "$@"
cat <<EOF

Manual git installation.

    jhinst git <Version> <Folder>

To find available versions (e.g. 2.15.0), please visit:
    https://mirrors.edge.kernel.org/pub/software/scm/git/

The Folder is typically "${HOME}/.local".
Whatever you choose, if it doesn't exist, it will be created.

EOF
exit 0
}

# extract inputs
[ $# -lt 2 ] && usage 
Version=$1
Target=$(rtrim_slash "$2")

# create target folder
Download=${Target}/src
[ ! -d "$Download" ] && mkdir -pv "$Download"
[ ! -d "$Download" ] && echoerrx1 "Could not create folder: $Download"
qpushd "$Download"

# download version
Name=git-${Version}
File=${Name}.tar.gz 
wget https://mirrors.edge.kernel.org/pub/software/scm/git/$File
[ -f "$File" ] || echoerrx1 "Could not download: $File"

# uncompress and move to extracted folder
tar xzf $File

Folder="${Download}/$Name"
[ ! -d "$Folder" ] && echoerrx1 "Couldn't find the extracted folder: $Folder"
qpushd "$Folder"

# run configuration
./configure --prefix="$Target"

# display message about what to do next
msg_bG "Dowloaded and configured git version $Version for installation in: $Target"
msg_G  "To proceed, run:"
msg_G  "    cd '$Folder'"
msg_G  "    make && make install"
