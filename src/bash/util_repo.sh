
# Technically these should be in app_git.sh 
# But git is obviously a command on the path, and these functions are used
# throughout the library...

# ------------------------------------------------------------------------

# Print status showing what to do
gits() {
    git branch -vv # show the current branch
    git fetch --quiet
    #git fetch --all --quiet

    if [[ `git status -uno --porcelain` ]]; then
        msg_bR "\t COMMIT needed"
    elif [[ `git status --porcelain` ]]; then
        msg_bY "\t Untracked files"
    elif [[ `git log @{u}..` ]]; then
        msg_bM "\t PUSH needed"
    elif [[ `git rev-parse @` == `git rev-parse @{u}` ]]; then
        msg_bG "\t All good"
    else
        msg_bC "\t PULL needed"
    fi
}

gitn() {
#
# 0: all good
# 1: pull
# 2: push
# 3: commit
# 4: untracked

    git fetch --quiet
    #git fetch --all --quiet

    if [[ `git status -uno --porcelain` ]]; then
        echo 3
    elif [[ `git status --porcelain` ]]; then
        echo 4
    elif [[ `git log @{u}..` ]]; then
        echo 2
    elif [ `git rev-parse @` = `git rev-parse @{u}` ]; then
        echo 0
    else
        echo 1
    fi
}

# Set remote URL or create remote
git_remote() {
    (( $# != 2 )) && { echo "Usage: $0 <remote_name> <remote_url>"; return; }

    if [[ $(git config remote.${1}.url) ]]; then
        git remote add $1 $2
    else
        git remote set-url $1 $2
    fi
}

# show branches and their upstreams
# see: https://serverfault.com/a/175083/179285
git_showup() {
    # more portable version (https://stackoverflow.com/q/51536069/472610)
    #git branch "$@" --format='%(refname:short)  <-  %(upstream:short)'
    git for-each-ref --format='%(refname:short)  <-  %(upstream:short)' refs/heads
}

# show upstream of specified branch
# see: https://serverfault.com/a/384862/179285
git_upstream() {
    [ $# -lt 1 ] && { echoerr "Usage: git_upstream <Branch>"; return 1; }
    git rev-parse --symbolic-full-name --abbrev-ref "${1}@{u}"
}

# show just the current branch
# see: https://stackoverflow.com/a/1418022/472610
git_curbranch() {
    git rev-parse --abbrev-ref HEAD
}

# check that local branch exists
# see: https://stackoverflow.com/q/5167957/472610
git_lisbranch() {
    git show-ref --verify --quiet "refs/heads/$1"
    [ $? -eq 0 ] && echo 1
}

# List branches in a way that can be used for scripting
git_lsbr() {
    case ${1:-'local'} in
    -l|loc*)
        # git branch --format '%(refname:short)'
        git for-each-ref --format='%(refname:short)' refs/heads
        ;;
    -r|rem*)
        # for b in $(git branch -r --format '%(refname:short)'); do
        #     [[ "$b" != *'/HEAD' ]] && echo $b
        # done
        git for-each-ref --format='%(refname:short)' refs/remotes
        ;;
    *)
        echoerr "Unknown type: $1"
        ;;
    esac
}

# Weak test that folder is the root of a git repo
git_chkroot() {
    [ -d "$1" ] || { echoerr "Folder not found: $1"; return 1; }
    [ -s "$1/.git/HEAD" ] && [ -s "$1/.git/config" ] && echo 1
}

# remove branch remotely and locally
# see: https://stackoverflow.com/a/2003515/472610
# git_rmbranch() {}
#
# git push <Remote> --delete <Branch>
# git branch -d <Branch>


# Show the UTC date of last commit to specified file
# See:
#   https://stackoverflow.com/a/8611514/472610
#   http://schacon.github.io/git/git-log.html#_pretty_formats
git_mtime() {
    (( $# < 1 )) && { echo "Usage: $0 <filename>"; return; }
    TSTAMP=$(git log -1 --format=%ct "$1")
    echo $(date -u --date @$TSTAMP)
}
