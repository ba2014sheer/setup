
path_prepend() {
	local x=$(rtrim_slash "$1")
	[ -d "$1" ] && export PATH="${x}${PATH:+:$PATH}"
}

# see: https://unix.stackexchange.com/a/291611/45354
path_remove() {
	local x=$(rtrim_slash "$1")
	PATH=${PATH//":$x:"/":"} # in the middle
	PATH=${PATH/#"$x:"/} # at the beginning
	PATH=${PATH/%":$x"/} # at the end
	export PATH
}

path_print() {
	echo -e ${PATH//:/"\n"}
}

# ------------------------------------------------------------------------

pypath_append() {
	local x=$(rtrim_slash "$1")
	[ -d "$1" ] && export PYTHONPATH="${PYTHONPATH:+$PYTHONPATH:}${x}"
}

# see: https://unix.stackexchange.com/a/291611/45354
pypath_remove() {
	local x=$(rtrim_slash "$1")
	PYTHONPATH=${PYTHONPATH//":$x:"/":"} # in the middle
	PYTHONPATH=${PYTHONPATH/#"$x:"/} # at the beginning
	PYTHONPATH=${PYTHONPATH/%":$x"/} # at the end
	export PYTHONPATH
}

pypath_print() {
	echo -e ${PYTHONPATH//:/"\n"}
}

# ------------------------------------------------------------------------

lib_prepend() {
	local x=$(rtrim_slash "$1")
	case $JH_OS in
	OSX)
		[ -d "$x" ] && export DYLD_LIBRARY_PATH="${x}${DYLD_LIBRARY_PATH:+:$DYLD_LIBRARY_PATH}"
		;;
	Linux)
		[ -d "$x" ] && export LD_LIBRARY_PATH="${x}${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
		;;
	esac
}

lib_remove() {
	local x=$(rtrim_slash "$1")
	local y
	case $JH_OS in
	OSX)
		y=$DYLD_LIBRARY_PATH
		y=${y//":$x:"/":"} # in the middle
		y=${y/#"$x:"/} # at the beginning
		y=${y/%":$x"/} # at the end
		export DYLD_LIBRARY_PATH=$y
		;;
	Linux)
		y=$LD_LIBRARY_PATH
		y=${y//":$x:"/":"} # in the middle
		y=${y/#"$x:"/} # at the beginning
		y=${y/%":$x"/} # at the end
		export LD_LIBRARY_PATH=$y
		;;
	esac
}

lib_print() {
	case $JH_OS in
	OSX)
		echo -e ${DYLD_LIBRARY_PATH//:/"\n"}
		;;
	Linux)
		echo -e ${LD_LIBRARY_PATH//:/"\n"}
		;;
	esac
}
