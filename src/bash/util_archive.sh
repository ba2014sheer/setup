
# Creates an archive from given a list of directories
compress() {
    (( $# < 3 )) && { echo 'Usage: compress <extension> <output> <folders...>'; return; }

    local ext=$1
    local name=${2}.${ext}
    shift 2

    case $ext in 
    tar) 	
        tar cf "$name" "$@"      ;;
    tgz|tar.gz) 	
        tar czf "$name" "$@"     ;;
    tbz2|tar.bz2) 	
        tar cjf "$name" "$@"     ;;
    zip)  	
        zip -r "$name" "$@"       ;;
    *)	
        echoerr "Unknown extension '$ext'." ;;
    esac
}

# Uncompress an archive
extract() {
    (( $# < 1 )) && { echo 'Usage: extract <Filename> <args...>'; return; }

    local name=$1
    shift 

    if [ -f "$name" ]; then
        case $name in
        *.tar.*)       
            tar xf "$name" "$@"      ;;
            # -j for bz2
            # -z for gzip
        *.zip)       
            unzip "$name" "$@"        ;;
        *.7z)        
            7z x "$name" "$@"         ;;
        *.rar)       
            unrar x "$name" "$@"      ;;
        *.Z)         
            uncompress "$name" "$@"   ;;
        *)           
            echoerr "'$name' could not be extracted." ;;
        esac
    else
        echoerr "'$name' is not a valid file!"
        return 1
    fi
}

# test tar file integrity
# see: https://unix.stackexchange.com/a/129600/45354
tarhealth() {
    for x in "$@"; do 
        if ! tar tf "$x" &>/dev/null; then 
            msg_R "$x"
        else 
            msg_G "$x"
        fi
    done
}
