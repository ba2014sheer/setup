
# Bash utilities

## Packages

Tools should warn about required packages that are not installed (see [requirements](./require.sh)).

Basically:
```
coreutils findutils diffutils
openssl ssh-copy-id
git rsync wget curl
```

Desirable packages:
```
bsdmainutils xxd hexdump pv srm 
```


## Dev notes

```
mv
    Takes -t option in Linux (for target folder), but moves to the last argument instead in OSX.

sed
    In-place sed doesn't work the same in Linux and OSX.

ncpus, loadavg
    Linux distributions using procfs can read /proc, but OSX does not.
    Instead, use sysctl (use -a to list all, and -n to get value of one).
```

On OSX, installing `coreutils` with Homebrew means that all utilities are prefixed with "g" (e.g. `date -> gdate`).
