
pdf_extract() {
    (( $# < 3 )) && { echo "Usage: $0 <input-file> <first-page> <last-page> <output_file>"; return; }
    [ -z "$(iscmd gs)" ] || { echoerr "This command requires ghostscript, but it is not installed."; return 1; }

    OUTFILE=${1%.pdf}_p${2}-p${3}.pdf
    (( $# > 3 )) && OUTFILE=$4

    gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dSAFER \
        -dFirstPage="$2" \
        -dLastPage="$3" \
        -sOutputFile="$OUTFILE" \
        "$1"

    echo "Output saved in: $OUTFILE"
}
[ -z "$(iscmd gs)" ] && { unset -f pdf_extract; echoinf '[app_pdf] ghostscript missing'; }


pdf2img() {
    (( $# < 2 )) && { echo "Usage: $0 <Input> <Output> <DPI:150> <Quality:90>"; return; }
    [ -z "$(iscmd convert)" ] && { echo "ImageMagick is required for this command."; return 1; }

    DPI=${3:-'150'}
    QUALITY=${4:-'90'}

    convert -density "$DPI" "$1" -quality "$Quality" "$2"
}
[ -z "$(iscmd convert)" ] && { unset -f pdf2img; echoinf '[app_pdf] imagemagick missing'; }