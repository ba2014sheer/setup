
# shred command for util_crypto
# See: https://pthree.org/2011/03/09/various-ways-to-shred-a-drive/
# See: https://wiki.archlinux.org/index.php/Securely_wipe_disk

# implement pdfmerge and pdfsplit in Python module
# see: https://stackoverflow.com/a/37945454/472610
# see: https://stackoverflow.com/a/45145249/472610

# adapt from OSX 
#   say
#   funfacts

# Start a web server to share the files in the current directory
sharefolder() { http-server "${1:-.}" -p "${2:-666}" ;}
