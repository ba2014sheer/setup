
ncpus() {
	case $JH_OS in
	OSX)
		echo $(sysctl -n hw.ncpu)
		;;
	Linux)
		echo $(nproc --all)
		;;
	esac
}

# Compute the load average
loadavg() {
	local u=$(uptime)
	local p=$(ncpus)

	# substring after last ':'
	u=(${u##*:})

	# divide each load (1min, 5min, 15min) by the number of processors
	for i in "${!u[@]}"; do
		u[$i]=$(filter_dec "${u[$i]}")
		u[$i]=$(calc "int(${u[$i]} / $p)")
	done

	# print the normalised load averages
	echo "${u[@]}"
}

# find unused network port
# see: https://unix.stackexchange.com/a/132524/45354
freeport() {
	python -c 'import socket; s=socket.socket(); s.bind(("", 0)); print(s.getsockname()[1]); s.close()'
}

# find linux distribution
linux_distro() {
	# see: https://unix.stackexchange.com/a/195808/45354
	local d=$(python2 -c "import platform; print platform.platform()")

	# warn if not linux
	[[ "$d" != Linux* ]] && { echoerr "Not a Linux system: $d"; return; }

	# find the portion after ..-with-?
	d=${d#*-with-}
	case $d in 
	debian) 	echo debian 		 			;;
	ubuntu) 	echo ubuntu 		 			;;
	centos) 	echo centos 					;;
	redhat) 	echo redhat 		 			;;
	*)  		echoerr "Unknown distro: $d" 	;;
	esac
}
