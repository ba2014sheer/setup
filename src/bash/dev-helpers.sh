
echo "Not supposed to be executed!!"
exit 1;

# path to folder containing currently executing script
$( cd "$( dirname "${BASH_SOURCE}" )" && pwd -P )

# test whether this is a bash terminal
[ -n "$BASH_VERSION" ]

# whether there is display capability
[ -n "$DISPLAY" ]

# test for file/folder as a command
mytest() { 
    test "$@" 
    (( $? == 0 )) && echo 1
}

# whether the current script's output is bound to a terminal 
# (as opposed to being piped or subshelled for example)
# see: https://stackoverflow.com/a/30520299/472610
[ -t 1 ] 

# check if user is root
# see: https://stackoverflow.com/a/28776100/472610
[ $(id -u) -eq 0 ]

# is program callable (i.e. does it exist / is it on the path?)
# dont forget outside quotes!
[ -n "$(iscmd "$1")" ]

# ask question
read -ep 'Do you want to? [y/N] ' response
case $response in
    [Yy]* )
        echo 'Doing it!'
        ;;
    * )
        echo 'Abort'
        ;;
esac

# auto-complete command (example)
_git_checkout_branch_complete() {
    local cur prev opts
    COMPREPLY=()
    cur=${COMP_WORDS[COMP_CWORD]}
    prev=${COMP_WORDS[COMP_CWORD-1]}

    opts=$(git branch | cut -c 3-)

    COMPREPLY=( $(compgen -W "$opts" -- $cur) )
}
complete -o nospace -F _git_checkout_branch_complete gitc
complete -o nospace -F _git_checkout_branch_complete gitdb

# Git identity manager: https://github.com/samrocketman/git-identity-manager