
# check that essential commands are there
_check_err() {
    if [[ $(nocmd "$1") ]]; then 
        echoerr "Command missing: $1"
        [ $# -gt 1 ] && { shift; echoerr "\t$*"; }
    fi
}

# common ones
_check_err "git"

# OS-specific messages
case $JH_OS in
OSX)
    _check_err "brew" "Install with: jhinst brew"
    _check_err "gdate" "Install with: brew install coreutils"
    _check_err "gfind" "Install with: brew install findutils"
    _check_err "openssl" "Install with: brew install openssl"
    _check_err "rsync" "Install with: brew install rsync"
    _check_err "wget" "Install with: brew install wget"
    _check_err "curl" "Install with: brew install curl"
    _check_err "ssh-keygen" "Install with: brew install openssh"
    ;;
Linux)
    _check_err "rsync"
    _check_err "openssl"
    _check_err "wget"
    _check_err "curl"
    _check_err "ssh-keygen"
    ;;
esac

unset -f _check_err
