
# Should work fine even if tput is not defined
if [[ "$(nocmd tput)" ]]; then
    NColors=0
else
    NColors=$(tput colors)
fi

if (( $NColors >= 8 )); then 
    # Colors
    ColK=$(tput setaf 0)
    ColR=$(tput setaf 1)
    ColG=$(tput setaf 2)
    ColY=$(tput setaf 3)
    ColB=$(tput setaf 4)
    ColM=$(tput setaf 5)
    ColC=$(tput setaf 6)
    ColW=$(tput setaf 7)

    # Backgrounds
    OnK=$(tput setab 0)
    OnR=$(tput setab 1)
    OnG=$(tput setab 2)
    OnY=$(tput setab 3)
    OnB=$(tput setab 4)
    OnM=$(tput setab 5)
    OnC=$(tput setab 6)
    OnW=$(tput setab 7)

    # Effects
    FontK=$(tput blink)
    FontB=$(tput bold)

    # Reset
    FontD=$(tput sgr0)
fi

# Use like:
# printf '%s\n' "${ColR}${FontB}${OnK}My text${FontD}"

# Basic color print
msg_K() { echo -e "${ColK}$*${FontD}"; }
msg_R() { echo -e "${ColR}$*${FontD}"; }
msg_G() { echo -e "${ColG}$*${FontD}"; }
msg_Y() { echo -e "${ColY}$*${FontD}"; }
msg_B() { echo -e "${ColB}$*${FontD}"; }
msg_M() { echo -e "${ColM}$*${FontD}"; }
msg_C() { echo -e "${ColC}$*${FontD}"; }
msg_W() { echo -e "${ColW}$*${FontD}"; }

# Bold message
msg_b() { echo -e "${FontB}$*${FontD}"; }

# Basic bold color print
msg_bK() { echo -e "${FontB}${ColK}$*${FontD}"; }
msg_bR() { echo -e "${FontB}${ColR}$*${FontD}"; }
msg_bG() { echo -e "${FontB}${ColG}$*${FontD}"; }
msg_bY() { echo -e "${FontB}${ColY}$*${FontD}"; }
msg_bB() { echo -e "${FontB}${ColB}$*${FontD}"; }
msg_bM() { echo -e "${FontB}${ColM}$*${FontD}"; }
msg_bC() { echo -e "${FontB}${ColC}$*${FontD}"; }
msg_bW() { echo -e "${FontB}${ColW}$*${FontD}"; }

# strip coloring markup from string
stripcolors() {
	case $JH_OS in
    OSX)
        sed -E "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"
        ;;
    Linux)
        sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g"
        ;;
	esac
}
