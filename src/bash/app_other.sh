
# show the weather in terminal
weather() { curl "http://wttr.in/${1:-oxford}?lang=en"; }
[ -z "$(iscmd curl)" ] && { unset -f weather; echoinf '[app_other] curl missing'; }

# check if a URL is reachable
#
# additional inputs can be:
#   --tries 10
#   --timeout 20  (in sec)
url_reachable() {
    local URL="$1"
    shift
    wget -q "$@" --spider "$URL" && echo 1
}
