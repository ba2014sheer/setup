
# Compile LaTeX with/without bibliography
ctex() {
    [ ! -f "$1" ] && { echo "File not found: $1"; return 1; }
    [ ! -d _out ] && mkdir _out
    pdflatex -output-directory=./_out "$1"
}

cbtex(){
    [ ! -f "$1" ] && { echo "File not found: $1"; return 1; }
    [ ! -d _out ] && mkdir _out
    pdflatex -output-directory=./_out "$1"
    bibtex "_out/${1%.*}"
    pdflatex -output-directory=./_out "$1"
    pdflatex -output-directory=./_out "$1"
}
