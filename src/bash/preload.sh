
#
# Essential things with no dependency
#

# include things from this folder
_jhb_inc() { source "${JH_SRC}/bash/${1}.sh" ;}

# check command exists, or show error message
# NOTE: which outputs annoying error message on CentOS
iscmd() { [ -n "$(which "$1" 2>/dev/null)" ] && echo 1; }
nocmd() { [ -z "$(which "$1" 2>/dev/null)" ] && echo 1; }

# check function exists in current shell
isfun() { [[ $(type -t "$1") == 'function' ]] && echo 1; }
nofun() { [[ $(type -t "$1") == 'function' ]] || echo 1; }

# quiet pushd/popd
# see command dirs for current folder stack
qpushd() { pushd "$@" >/dev/null; } 
qpopd()  { popd "$@" >/dev/null; } 

# test if a variable is set (even if null)
# see: https://stackoverflow.com/a/51425455/472610
isunset() { [[ "${!1}" != 'x' ]] && [[ "${!1-x}" == 'x' ]] && echo 1; }
isset()   { [ -z "$(isunset "$1")" ] && echo 1; }
