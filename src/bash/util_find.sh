
# Find a file with a pattern in name:
ff() { find . -type f -iname '*'"$1"'*' -ls ;}
fd() { find . -type d -iname '*'"$1"'*' -ls ;}

# find deadlinks
deadlinks() { find . "$*" -xtype l; }

# Find a file with pattern $1 in name and Execute $2 on it:
# fe() { find . -type f -iname '*'"${1:-}"'*' -exec ${2:-file} {} \;  ; }

# Iterate on the results of find should not be done with for loops (see http://mywiki.wooledge.org/BashPitfalls#for_i_in_.24.28ls_.2A.mp3.29)
# Instead should be done like this:
# find . -type d -perm 777 -print0 | while IFS= read -r -d '' d; do chmod 755 "$d"; done
# See also: http://mywiki.wooledge.org/BashFAQ/001

# Find a pattern in a set of files and highlight them
# For a unique list of files, simply use option: -l
fstr() {
	if [ $# -lt 1 ]; then
		echo 'Find pattern in files. Usage: fstr "pattern" [grep arguments]'
		return
	else
		pattern=$1
		shift 1
		other="$@"
	fi
	echo "Searching for '$pattern' with option(s) '$other'..."
	grep --color=auto $other -Ern . -e "$pattern"
}

# fstr_list() {
# 	fstr "$@" | tail -n +2 | awk -F ':' '{ print $1 }' | sort -u 
# 	# remove 'Searching for ...'
# 	# keep only filename (before first ':')
# 	# unique matches only
# }

fstr_file() {
	if [ $# -lt 2 ]; then
		echo 'Find pattern in specific file. Usage: fstr_file "file" "pattern" [grep arguments]'
		return
	else
		file=$1
		pattern=$2
		shift 2
	fi
	echo "Searching for '$pattern' in file '$file' with options '$@'..."
	grep --color=auto "$@" -En "$file" -e "$pattern"
}

# Find and replace a pattern with another
frep() {
	if [ $# -lt 2 ]; then
		echo 'Find and replace a string in files. Usage: frep "old" "new" [grep arguments]'
		return
	else
		old=$1
		new=$2
		shift 2
	fi
	echo "Replacing '$old' with '$new' using grep options '$@'..."

	grep "$@" -Erl . -e "$old" | while IFS= read -r file; do
		echo -e "\t Editing: '$file'"
		insed "$old" "$new" "$file"
	done
}

frep_file() {
	if [ $# -lt 3 ]; then 
		echo 'Alias for command insed. Usage: frep_file "file" "old" "new"'
		return
	else 
		file=$1
		old=$2
		new=$3
	fi
	echo "Replacing '$old' with '$new' in file '$file'..."
	insed "$old" "$new" "$file"
}
