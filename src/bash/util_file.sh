
# wrapper for the test command
# returns 1 on success, nothing otherwise
filetest() {
    test "$@"
    (( $? == 0 )) && echo 1;
}

# Swap two files or folders
fileswap() { 
    local TempFile=tmp.$$

    (( $# != 2 )) && { echo "Usage: swap <File1> <File2>"; return; }
    [ -e "$TempFile" ] && { echoerr "swap: (bug) tempfile already exists"; return 1; }
    [ ! -e "$1" ] && { echoerr "swap: '$1' does not exist"; return 1; }
    [ ! -e "$2" ] && { echoerr "swap: '$2' does not exist"; return 1; }

    mv "$1" $TempFile
    mv "$2" "$1"
    mv $TempFile "$2"
}

# bash-only solution to determine real path
abspath() {
    if [ $# -eq 0 ]; then 
        echo $( pwd -P )
    elif [ -d "$1" ]; then
        echo $( cd "$1" && pwd -P )
    else
        echo $( cd "$( dirname "$1" )" && pwd -P )
    fi
}

# requires a folder input
absdir() {
    [ $# -lt 1 ] && { echoerr "Usage: absdir path"; return; }
    [ ! -d "$1" ] && { echoerr "Not a folder: $1"; return 1; }
    echo $( cd "$1" && pwd -P )
}

# # use Python to determine real path
# abspath() {
#     local target=${1:-'.'}
#     python2 -c "import os,sys; print os.path.abspath('$target')"
# }
# [ -z "$(iscmd python2)" ] && { unset -f abspath; echoinf '[util_file] python2 missing'; }

# remove symlink (override built-in)
unlink() {
    (( $# == 0 )) && { echo "Usage: unlink <link1> <link2> ..."; return; }
    for L in "$@"; do 
        L=${L%/} # no trailing slash
        [ ! -L "$L" ] || rm -f "$L"
        [ ! -e "$L" ] || echoerr "Not a symlink: $L"
    done
}

# relink symlinks
relink() {
    (( $# != 2 )) && { echo "Usage: relink <link_name> <new_target>"; return; }
    Linkname=${1%/} # no traling slash
    unlink "$Linkname"
    [ -e "$Linkname" ] && { echoerr "Target exists: $Linkname"; return 1; }
    ln -sv "$2" "$Linkname"
}

# Size of a folder
dirsize() { du -hs "${1:-$HOME}/*" | sort -nr ;} # recursively search
dirfree() { df -hP "${1:-$PWD}" | tail -n +2 | awk '{print $5}' ;} # show free percentage
dirnonempty() { [ -d "$1" ] && [ -n "$(ls "$1")" ] && echo 1; }
dirempty() { [[ $(dirnonempty "$1") ]] || echo 1; }

# Change directory to the actual target path
acd() { cd "$1"; cd "$(pwd -P)"; }
cs() { cd "$1"; ls; }
cl() { cd "$1"; ll; }

# Move to trash instead of deleting
trash() {
    local TrashDir=${JH_TRASH}
    [ ! -d "$TrashDir" ] && mkdir -pv "$TrashDir"
    
    case $JH_OS in
        OSX)
            mv "$@" "${TrashDir}/"
            ;;
        Linux)
            mv -t "${TrashDir}/" "$@"
            ;;
    esac
}

# backup file/folder to specified folder
backup() {
    (( $# < 2 )) && { echo "Usage: backup <Folder> <Files..>"; return; }
    local Folder=$(rtrim_slash "$1")
    local Stamp=$(timestamp)

    [ ! -d "$Folder" ] && { echoerr "Folder not found: $Folder"; return 1; }
    shift
    for f in "$@"; do 
        f=$(rtrim_slash "$f") # allow folders to be backed up too
        if [ -e "$f" ]; then
            mv -v "$f" "${Folder}/${Stamp}-$(basename $f)"
        else
            echoerr "File not found: $f"
        fi
    done
}

# copy file and append timestamp to name
backfile() {
    local File=$1
    local Dest=${2:-"$1-%s"}
    if [ -f "$File" ] || [ -L "$File" ]; then 
        Dest=$(printf  "$Dest" `timestamp`)
        mv -v "$File" "$Dest"
    fi
}

# rm -i annoyingly asks for each subfolder...
# ask confirmation once before removing entire folder
removedir() {
    local Folder=$1
    [ ! -d "$Folder" ] && { echo "Not a folder: $Folder"; return 1; }
    [[ $(confirm "Delete folder '$Folder'?") ]] && rm -rf "$Folder"
}

# extract line from file
fileline() {
    (( $# < 2 )) && { echo "Usage: fileline <File> <Line>"; return; }
    sed "${2}q;d" "$1"
}

# iterate the line of a file and call input function
iterlines() {
    (( $# < 2 )) && { echo "Usage: iterlines <File> <Callback>"; return; }
    local File=$1
    local Func=$2
    n=$(cat "$File" | wc -l)
    for (( i=1; i<=n; i++ )); do
        "$Func" "$(sed "${i}q;d" "$File")"
    done
}

# convert CRLFs to LFs
win2nix() { perl -pi -e "s/\r\n$/\n/g" "$1"; }
nix2win() { perl -pi -e "s/\n$/\r\n/g" "$1"; }

# compare lines between files
lcmp() {

if [ $# -lt 3 ]; then 
cat <<EOF 
    Usage: lcmp <Mode> <FileA> <FileB>

    Compare the lines of two files.

    Modes are:
        AnotB   Find lines in A but not in B
        BnotA   Find lines in B but not in A
        AandB   Fine common lines to A and B

EOF
return
fi

    local Mode
    case $1 in 
    AnotB|A)    
        Mode=-23
        ;;
    BnotA|B)
        Mode=-13
        ;;
    AandB|BandA|AB)
        Mode=-12
        ;;
    *)
        echoerr "Unknown mode: $1"
    esac

    comm $Mode <(sort -u "$2") <(sort -u "$3")
}
