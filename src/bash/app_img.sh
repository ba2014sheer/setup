
[ -z "$(iscmd convert)" ] && { echoinf '[app_img] ImageMagick missing'; return; }

img_size() {
    identify "$1" | awk '{print $3}'
}

img_pad() {
    local width height pw ph 
    [ $# -lt 3 ] && { echo "Usage: img_pad <Input> <PadWidthxPadHeight> <Output>"; return; }

    IFS=x read width height <<< "$(img_size "$1")"
    IFS=x read pw ph <<< "$2"

    width=$(calc "$width + 2*$pw")
    height=$(calc "$height + 2*$ph")

    echo "Converting '$1' to '$3' with size ${width}x${height} (+w=${pw}, +h=${ph})."
    convert "$1" -gravity center -extent "${width}x${height}" "$3"
}

# make white background transparent
img_tbg() {
    local fuzz
    [ $# -lt 2 ] && { echo "Usage: img_tbg <Input> <Output> <Fuzz=0>"; return; }
    fuzz=${3:-0}

    if (( $fuzz > 0 )); then
        echo "Making white background transparent in '$1'."
        convert "$1" -transparent white "$2"
    else 
        echo "Making white background transparent in '$1' (fuzz=${fuzz}%)."
        convert "$1" -fuzz ${fuzz}% -transparent white "$2"
    fi
}
