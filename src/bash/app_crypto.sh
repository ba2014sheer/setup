
# Random hash code
# See: https://stackoverflow.com/q/34328759/472610
randhash() {
	local L=${1:-16}
	echo $(head -c $((L/2+1)) </dev/urandom | xxd -p | head -c $L)
}
[ -z "$(iscmd xxd)" ] && { unset -f randhash; echoinf '[app_other] xxd missing'; }

# Encryption
fencrypt() { 
	[ $# -lt 1 ] && { echo "  fencrypt <File>"; return; }
	openssl aes-256-cbc -in "$1" -a > "$1.enc"
}

fdecrypt() { 
	[ $# -lt 1 ] && { echo "  fdecrypt <File> <Output: less>"; return; }
	if [ $# -gt 1 ]; then
		openssl aes-256-cbc -d -in "$1" -a > "$2"
	else
		openssl aes-256-cbc -d -in "$1" -a | less 
	fi
}
