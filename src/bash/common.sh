
# display info in yellow, unless there is no display
echoinf() { [ -n "$JH_FLAG_DISPLAY" ] && msg_Y "$*"; }

# echo to stderr
# see: https://stackoverflow.com/a/23550347/472610
echoerr()   { >&2 msg_R "$*"; }
echoerrx1() { echoerr "$*"; exit 1; }
echoerrx0() { echoerr "$*"; exit 0; }

# Allow floating-point arithmetic using awk
calc() { awk "BEGIN { pi = 3.14159265; srand(); print $* }"; }
randf() { 
    [ $# -lt 2 ] && { echo "Usage: randf lo up n=1"; return; }
    local lo=$1
    local up=$2
    local n=${3:-1}
    awk "BEGIN { srand(); for (i=1; i<=$n; i++) print( $lo + ($up - $lo)*rand() ); }"
}
randi() { 
    [ $# -lt 2 ] && { echo "Usage: randi lo up n=1"; return; }
    local lo=$1
    local up=$2
    local n=${3:-1}
    awk "BEGIN { srand(); for (i=1; i<=$n; i++) print int( $lo + ($up - $lo)*rand() ); }"
}

# join inputs with a specified separator
join() { local IFS="$1"; shift; echo "$*"; }

# Slahes at the end of paths
rtrim_slash() { echo "$1" | sed 's/\/*$//g'; }
force_slash() {	echo $(rtrim_slash "$1")/; }

# test if string has spaces
# see: https://stackoverflow.com/a/1475253/472610
has_space()   { [[ "$1" != "${1%[[:space:]]*}" ]] && echo 1; }
has_nospace() { [[ "$1" == "${1%[[:space:]]*}" ]] && echo 1; }

# various string filters
filter_dec() { echo "$1" | sed 's/[^0-9\.]*//g'; }
isdigits() { [[ "$1" == "$(filter_dec "$1")" ]] && echo 1; }

# trim leading and trailing whitespace
# see: https://unix.stackexchange.com/a/205854/45354
strtrim() { awk '{$1=$1};1' <<< "$@"; }

# datetime returns a human-readable date
# timestamp returns a sortable date
# both methods take the TZ as an argument (see /usr/share/zoneinfo/)
datetime()  { TZ="${1:-UTC}" date +"%a %d %b %Y, %H:%M:%S (${TZ})" ;}
timestamp() { TZ="${1:-UTC}" date +"%y%m%d_%H%M%S" ;}

# In-place sed is different in OSX and Linux
insed() {
    (( $# < 3 )) && { echo "Usage: inset <FindPattern> <Replace> <File>"; return; }
    case $JH_OS in
    OSX)
        LC_ALL=C sed -i '' -E "s/$1/$2/g" "$3"
        ;;
    Linux)
        sed --in-place --regexp-extended "s/$1/$2/g" "$3"
        ;;
    esac
}

# ask a question
confirm() {
    local msg=$(echo -e "$*")
    read -rep "$msg [y/N] " ans
    case $ans in 
        [yY][eE][sS]|[yY]) 
            echo 1
            return 0
            ;;
        *)
            echoerr 'Cancelled'
            return 1
            ;;
    esac
}

acknowledge() {
    local msg=$(echo -e "$*")
    read -n1 -rsp "$msg [Press ENTER to continue] "
    echo
}
