
# find pattern in array, echo matching indices
# 1: pattern (string, can contain glob)
# 2: expanded array 
arrayFind() {
    local pat=$1
    shift
    local arr=( "$@" )
    local match=()
    for k in ${!arr[@]}; do
        [[ "${arr[$k]}" == $pat ]] && match+=( $k )
    done
    echo "${match[@]}"
}

# echo 1 if array has value
# 1: pattern (string, can contain glob)
# 2: expanded array
arrayHas() {
    local pat=$1
    shift
    for val in "$@"; do
        [[ "$val" == $pat ]] && { echo 1; return 0; }
    done
    return 1
}

# filter the elements of an array according to pattern
# 1: pattern
# 2: array
arrayKeep() {
    local pat=$1
    local out=()
    shift
    for val in "$@"; do 
        [[ "$val" == $pat ]] && out+=( "$val" )
    done
    #printf "%s\n" "${out[@]}"
    #echo "${out[@]}"
    printf "%s\n" "${out[@]}"
}
arrayRemove() {
    local pat=$1
    local out=()
    shift
    for val in "$@"; do 
        [[ "$val" != $pat ]] && out+=( "$val" )
    done
    printf "%s\n" "${out[@]}"
}

# filter the elements of an array according to predicate
arrayAccept() {
    local fun=$1
    local out=()
    shift
    for val in "$@"; do 
        [[ $($fun "$val") ]] && out+=( "$val" )
    done
    printf "%s\n" "${out[@]}"
}
arrayReject() {
    local fun=$1
    local out=()
    shift
    for val in "$@"; do 
        [[ $($fun "$val") ]] || out+=( "$val" )
    done
    printf "%s\n" "${out[@]}"
}

# read array from file
readArrayFromInlineFile() {
    unset $1
    eval "$1=( $(cat "$2") )"
}

readArrayFromMultilineFile() {
    unset $1
    IFS=$'\n' read -d '' -ra $1 < "$2"
}

readArrayFromMutilineOutput() {
    unset $1
    IFS=$'\n' read -d '' -ra $1 <<< "$2"
}

# shorter aliases
alias arrayJoin='join'
alias aread-fwords='readArrayFromInlineFile' 
alias aread-flines='readArrayFromMultilineFile' 
alias aread-olines='readArrayFromMutilineOutput' 