#!/bin/bash

ColR=$(tput setaf 1)
ColG=$(tput setaf 2)
ColB=$(tput setaf 4)
Col0=$(tput sgr0)

echoR() { >&2 echo -e "${ColR}$*${Col0}"; }
echoG() { echo -e "${ColG}$*${Col0}"; }
echoB() { echo -e "${ColB}$*${Col0}"; }

usage() {
[ $# -gt 0 ] && { echoR "$*"; exit 1; }
cat <<EOF 

    Description
EOF
exit 0;
}

# ------------------------------------------------------------------------

# extract arguments
[ $# -lt 1 ] && usage 
Argument=$1

# do stuff
