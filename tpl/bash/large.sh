#!/bin/bash

# load jh library
[ -z "$JH_ROOT" ] && JH_ROOT=~jhadida/.jh
[ -d "$JH_ROOT" ] || { echo "jh root not found: $JH_ROOT"; exit 1; }
source "$JH_ROOT/inc/scriptutils.sh" || { echo "jh lib not sourced"; exit 1; }

# usage
usage() {
[ $# -gt 0 ] && echoerr "$*"
cat <<EOF 

    mytool <cmd> <arg...> [opt...]

Description.

Commands:

    name
        args: -
        Description

EOF
exit 0;
}

# ------------------------------------------------------------------------

# extract arguments
[ $# -lt 1 ] && usage 
Command=$1

# parse commands and do stuff
case $Command in 
name)
    ;;

*)
    usage "Unknown command: $Command"
    ;;
esac
