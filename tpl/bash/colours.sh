#
# Just define colours and basic commands..
#

# Colors
ColK=$(tput setaf 0)
ColR=$(tput setaf 1)
ColG=$(tput setaf 2)
ColY=$(tput setaf 3)
ColB=$(tput setaf 4)
ColM=$(tput setaf 5)
ColC=$(tput setaf 6)
ColW=$(tput setaf 7)

# Backgrounds
OnK=$(tput setab 0)
OnR=$(tput setab 1)
OnG=$(tput setab 2)
OnY=$(tput setab 3)
OnB=$(tput setab 4)
OnM=$(tput setab 5)
OnC=$(tput setab 6)
OnW=$(tput setab 7)

# Effects
FontK=$(tput blink)
FontB=$(tput bold)

# Reset
FontD=$(tput sgr0)

# Basic color print
msg_K() { echo -e "${ColK}$*${FontD}"; }
msg_R() { echo -e "${ColR}$*${FontD}"; }
msg_G() { echo -e "${ColG}$*${FontD}"; }
msg_Y() { echo -e "${ColY}$*${FontD}"; }
msg_B() { echo -e "${ColB}$*${FontD}"; }
msg_M() { echo -e "${ColM}$*${FontD}"; }
msg_C() { echo -e "${ColC}$*${FontD}"; }
msg_W() { echo -e "${ColW}$*${FontD}"; }

# Bold message
msg_b() { echo -e "${FontB}$*${FontD}"; }

# Basic bold color print
msg_bK() { echo -e "${FontB}${ColK}$*${FontD}"; }
msg_bR() { echo -e "${FontB}${ColR}$*${FontD}"; }
msg_bG() { echo -e "${FontB}${ColG}$*${FontD}"; }
msg_bY() { echo -e "${FontB}${ColY}$*${FontD}"; }
msg_bB() { echo -e "${FontB}${ColB}$*${FontD}"; }
msg_bM() { echo -e "${FontB}${ColM}$*${FontD}"; }
msg_bC() { echo -e "${FontB}${ColC}$*${FontD}"; }
msg_bW() { echo -e "${FontB}${ColW}$*${FontD}"; }

# echo to stderr
# see: https://stackoverflow.com/a/23550347/472610
echoerr()   { >&2 msg_R "$*"; }
echoerrx1() { echoerr "$*"; exit 1; }
echoerrx0() { echoerr "$*"; exit 0; }

# quiet pushd/popd
# see command dirs for current folder stack
qpushd() { pushd "$@" >/dev/null; } 
qpopd()  { popd "$@" >/dev/null; } 
