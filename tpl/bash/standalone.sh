#
# A standalone bash lib that can be easily copied and used.
#

# ------------------------------------------------------------------------
# preload.sh -_jhb_inc

# check command exists, or show error message
# NOTE: which outputs annoying error message on CentOS
iscmd() { [ -n "$(which "$1" 2>/dev/null)" ] && echo 1; }
nocmd() { [ -z "$(which "$1" 2>/dev/null)" ] && echo 1; }

# quiet pushd/popd
# see command dirs for current folder stack
qpushd() { pushd "$@" >/dev/null; } 
qpopd()  { popd "$@" >/dev/null; } 

# test if a variable is set (even if null)
# see: https://stackoverflow.com/a/51425455/472610
isunset() { [[ "${!1}" != 'x' ]] && [[ "${!1-x}" == 'x' ]] && echo 1; }
isset()   { [ -z "$(isunset "$1")" ] && echo 1; }

# ------------------------------------------------------------------------
# colors.sh -stripcolors

# Should work fine even if tput is not defined
if [[ "$(nocmd tput)" ]]; then
    NColors=0
else
    NColors=$(tput colors)
fi

if (( $NColors >= 8 )); then 
    # Colors
    ColK=$(tput setaf 0)
    ColR=$(tput setaf 1)
    ColG=$(tput setaf 2)
    ColY=$(tput setaf 3)
    ColB=$(tput setaf 4)
    ColM=$(tput setaf 5)
    ColC=$(tput setaf 6)
    ColW=$(tput setaf 7)

    # Backgrounds
    OnK=$(tput setab 0)
    OnR=$(tput setab 1)
    OnG=$(tput setab 2)
    OnY=$(tput setab 3)
    OnB=$(tput setab 4)
    OnM=$(tput setab 5)
    OnC=$(tput setab 6)
    OnW=$(tput setab 7)

    # Effects
    FontK=$(tput blink)
    FontB=$(tput bold)

    # Reset
    FontD=$(tput sgr0)
fi

# Use like:
# printf '%s\n' "${ColR}${FontB}${OnK}My text${FontD}"

# Basic color print
msg_K() { echo -e "${ColK}$*${FontD}"; }
msg_R() { echo -e "${ColR}$*${FontD}"; }
msg_G() { echo -e "${ColG}$*${FontD}"; }
msg_Y() { echo -e "${ColY}$*${FontD}"; }
msg_B() { echo -e "${ColB}$*${FontD}"; }
msg_M() { echo -e "${ColM}$*${FontD}"; }
msg_C() { echo -e "${ColC}$*${FontD}"; }
msg_W() { echo -e "${ColW}$*${FontD}"; }

# Bold message
msg_b() { echo -e "${FontB}$*${FontD}"; }

# Basic bold color print
msg_bK() { echo -e "${FontB}${ColK}$*${FontD}"; }
msg_bR() { echo -e "${FontB}${ColR}$*${FontD}"; }
msg_bG() { echo -e "${FontB}${ColG}$*${FontD}"; }
msg_bY() { echo -e "${FontB}${ColY}$*${FontD}"; }
msg_bB() { echo -e "${FontB}${ColB}$*${FontD}"; }
msg_bM() { echo -e "${FontB}${ColM}$*${FontD}"; }
msg_bC() { echo -e "${FontB}${ColC}$*${FontD}"; }
msg_bW() { echo -e "${FontB}${ColW}$*${FontD}"; }

# ------------------------------------------------------------------------
# common.sh -info -insed

# echo to stderr
# see: https://stackoverflow.com/a/23550347/472610
echoerr()   { >&2 msg_R "$*"; }
echoerrx1() { echoerr "$*"; exit 1; }
echoerrx0() { echoerr "$*"; exit 0; }

# Allow floating-point arithmetic using awk
calc() { awk "BEGIN { print $* }"; }

# join inputs with a specified separator
join() { local IFS="$1"; shift; echo "$*"; }

# Slahes at the end of paths
rtrim_slash() { echo "$1" | sed 's/\/*$//g'; }
force_slash() {	echo $(rtrim_slash "$1")/; }

# test if string has spaces
# see: https://stackoverflow.com/a/1475253/472610
has_space()   { [[ "$1" != "${1%[[:space:]]*}" ]] && echo 1; }
has_nospace() { [[ "$1" == "${1%[[:space:]]*}" ]] && echo 1; }

# various string filters
filter_dec() { echo "$1" | sed 's/[^0-9\.]*//g'; }

# trim leading and trailing whitespace
# see: https://unix.stackexchange.com/a/205854/45354
strtrim() { awk '{$1=$1};1' <<< "$@"; }

# datetime returns a human-readable date
# timestamp returns a sortable date
# both methods take the TZ as an argument (see /usr/share/zoneinfo/)
datetime()  { TZ="${1:-UTC}" date +"%a %d %b %Y, %H:%M:%S (${TZ})" ;}
timestamp() { TZ="${1:-UTC}" date +"%y%m%d_%H%M%S" ;}

# ask a question
confirm() {
    read -rep "$* [y/N] " ans
    case $ans in 
        [yY][eE][sS]|[yY]) 
            echo 1
            return 0
            ;;
        *)
            echoerr 'Cancelled'
            return 1
            ;;
    esac
}

# ------------------------------------------------------------------------

# common checks needed in most scripts
chkfile() { [ -f "$1" ] || [ -L "$1" ] || echoerrx1 "File not found: $1"; }
chkexec() { [ -x "$1" ] || echoerrx1 "File not executable: $1"; }
chkdir() { [ -d "$1" ] || echoerrx1 "Folder not found: $1"; }
chkvar() { [ -n "${!1}" ] || echoerrx1 "Variable required: $1"; }
chkfun() { [[ $(type -t "$1") == 'function' ]] || echoerrx1 "Not a function: $1"; }
reqcmd() { [[ $(iscmd "$1") ]] || echoerrx1 "Command required: $1"; }

# mkdir checking if folder already exists, and exiting in case of failure
makedir() {
    local dpath=$1
    shift 1
    [ -d "$dpath" ] || \mkdir "$@" "$dpath" || echoerrx1 "Could not create folder: $dpath"
}
