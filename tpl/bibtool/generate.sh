#!/bin/bash

usage() {
cat <<EOF
Combine multiple bibtex files into a single one, taking care of duplicates 
and curating field information (see bibtoolrsc).

    generate  OutputFolder  OutputName=refs.bib
EOF
exit 0
}

[ $# -lt 1 ] && { echo "$0 <target_folder> <target_name>"; exit 1; }

SOURCE_FOLDER="$HOME/.mendeley"
SOURCE_FILES=(
    dphil.bib 
    Topics.bib
    Neuroscience.bib
)

TARGET_FOLDER=$1
TARGET_NAME=${2:-'refs.bib'}

if [ ! -d "$SOURCE_FOLDER" ]; then
    echo "Folder not found: $SOURCE_FOLDER"
else
    bibtool -d -o "${TARGET_FOLDER}/${TARGET_NAME}" \
        -s $(printf " ${SOURCE_FOLDER}/%s" "${SOURCE_FILES[@]}")
fi

