
# the base URL and the archive file to download, e.g.:
#   URL=https://gitlab.com/project/releases
#   Archive=name-v1.0.tar.gz
URL=
Archive=

# basic configure command -- make this as complicated as needed
ConfigCmd() {
    ./configure --prefix="$Prefix"
}

# display info after install (e.g. link to checkout new versions)
MoreInfo() {
    #msg_B "For more info, visit: $URL"
}
