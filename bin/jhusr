#!/bin/bash

# make sure the environment is setup
[ -z "$JH_ROOT" ] && { echo "Requires JH environment setup."; exit 1; }

# documentation
usage() {
[ $# -gt 0 ] && echoerr "$*"
cat <<EOF
JH's user interface.

    jhusr <Command> <Args...>


    Manage all users
    ----------------

    ls|list|all
        args: option
        List currently available users.
            -n  list names only
            -p  list full paths

    new|create
        args: name
        Create a new user.

    dl|clone|download
        args: url name
        Clone user config from repo.

    load|change|use
        args: name
        Load existing user.

    
    Manage current user repo
    ------------------------

    cur|current
        args: -
        Show name of current user.

    conf|config|env
        args: -
        Edit user config.

    rename
        args: name
        Rename current user.

    git
        args: ...
        Execute git command in user repo.

    status
        args: -
        Shortcut to show status of local user repo.
        Equivalent to: git status

    update|up
        args: -
        Shortcut to pull from remote user repo.
        Equivalent to: git pull --rebase

EOF
exit 0
}

# load utilities
UTIL="$JH_SRC/jh"
source "$UTIL/user-utils.sh"
FailBadge='jhusr'

# pop first input
[ $# -lt 1 ] && usage
Command="$1"
shift

# ---------------------------------------------------------------------

# process command
case "$Command" in

ls|list|all)
    case ${1:-'-n'} in 
    -n) 
        find "$JH_USER_STORE" -maxdepth 1 -type d ! -path "$JH_USER_STORE" -printf '%f\n'
        ;;
    -p) 
        find "$JH_USER_STORE" -maxdepth 1 -type d ! -path "$JH_USER_STORE"
        ;;
    esac
    ;;

new|create)
    [ $# -lt 1 ] && usage
    createUser "$1"
    ;;

cur|current)
    echo $(JH_getUserName)
    ;;

rename)
    [ $# -lt 1 ] && usage
    [[ $(has_space "$1") ]] && fail "Spaces not allowed in user name."
    echo "$1" >| "$JH_USR/$JH_USER_FILE"
    ;;

dl|clone|download)
    [ $# -lt 2 ] && usage
    git clone "$1" "$JH_USER_STORE/$2" || fail "Could not clone user config."
    ;;

conf|config|env)
    UserEnv="$JH_USR/$JH_USER_ENV"
    [ ! -f "$UserEnv" ] && cp -iv "$JH_TPL/jh/env" "$UserEnv"
    vim "$UserEnv"
    ;;

load|change|use)
    [ $# -lt 1 ] && usage
    Current=$(JH_getUserName)
    User=$1
    
    # if user is already loaded, exit
    if [[ "$Current" == "$User" ]]; then 
        msg_C "User already loaded: $User"
        exit 0
    fi

    # check user exists
    [[ $(userExists "$User") ]] || fail "User not found: $User"
    [[ $(userExists "$Current") ]] && fail "[bug] Duplicate user folder: $Current"

    # move folder
    msg_G "Storing user: $Current"
    \mv "$JH_USR" "$JH_USER_STORE/$Current"
    msg_G "Loading user: $User"
    \mv "$JH_USER_STORE/$User" "$JH_USR"
    ;;

git)
    [ -d "$JH_USR" ] || fail "User folder not found: $JH_USR"
    qpushd "$JH_USR" && git "$@"
    ;;

status|stat)
    qpushd "$JH_USR" && git status
    ;;
update|up)
    qpushd "$JH_USR" && git pull --rebase
    ;;

# ------------------------------------------------------------------------

# unrecognised command
*)
    usage "Unknown command: $Command"

esac
