#!/bin/bash

# make sure the environment is setup
[ -z "$JH_ROOT" ] && { echo "Requires JH environment setup."; exit 1; }

# documentation
usage() {
[ $# -gt 0 ] && echoerr "$*"
cat <<EOF
Proxy calls for scripts in src/inst.

    jhinst <Command> <Args...>

Main software
-------------

    conda       ->    Install (mini | ana)conda
    brew        ->    Install Homebrew (OSX only)
    node        ->    Install NVM + Node.js + NPM
    git         ->    Compile git from source
    gcc         ->    Compile gcc from source
    hcp         ->    Install Caret and Connectome Workbench
    fsl         ->    Install FSL
    osl         ->    Install OSL 
    tree        ->    Compile GNU/tree from source
    mrtrix      ->    Compile from GitHub

Install packages
----------------

    pkg-node    ->    Install global node packages
    pkg-osx     ->    Install essential OSX packages

Browser tabs
------------

    bf          ->    Open brew formulas
    vscode      ->    Open VSCode download page
    atom        ->    Open Atom download page
    hyper       ->    Open Hyper download page
    mendeley    ->    Open Mendeley download page
    fuse,sshfs  ->    Open SSHFS/FUSE github page
    karabiner   ->    Open Karabiner download page
    scroll      ->    Open Scroll Reverser download page
    skim        ->    Open Skim PDF download page

EOF
exit 0
}

# source utilities
source "${JH_SRC}/bash/include-min.sh"

# useful paths
InstFolder="${JH_SRC}/inst"

# pop first input
[ $# -lt 1 ] && usage
Command="$1"
shift

# define alias of "open" on Linux (see also: src/profile/alias.sh)
case $JH_OS in 
Linux)
    open() { xdg-open "$@"; }
    ;;
esac 

# ---------------------------------------------------------------------

# process command
case "$Command" in

conda)  
    "$InstFolder/conda.sh" "$@"
    ;;

brew|homebrew)
    "$InstFolder/brew.sh" "$@"
    ;;

node|nodejs|npm)
    "$InstFolder/node.sh" "$@"
    ;;

git)
    "$InstFolder/git.sh" "$@"
    ;;

gcc)
    "$InstFolder/gcc.sh" "$@"
    ;;

hcp|HCP)
    "$InstFolder/hcp.sh" "$@"
    ;;

tree)
    "$InstFolder/tree.sh" "$@"
    ;;

fsl|FSL)
    "$InstFolder/fsl.sh" "$@"
    ;;

osl|OSL)
    "$InstFolder/osl.sh" "$@"
    ;;

mrtrix|MRtrix)
    "$InstFolder/mrtrix.sh" "$@"
    ;;

# ---------------------------------------------------------------------

bf|brew-formulas)
    open "http://brewformulas.org/"
    ;;

vscode)
    # see also Anaconda script
    #   anaconda_dir/pkgs/vscode_inst.py
    open "https://code.visualstudio.com/Download"
    ;;

atom)
    # see also:
    #   https://github.com/atom/atom/releases/latest
    open "https://atom.io/"
    ;;

hyper)
    open "https://hyper.is/"
    ;;

mendeley)
    open "https://www.mendeley.com/download-desktop/"
    ;;

sshfs|fuse)
    open "https://osxfuse.github.io/"
    ;;

karabiner)
    open "https://pqrs.org/osx/karabiner/"
    ;;

scroll)
    open "https://pilotmoon.com/scrollreverser/"
    ;;

skim)
    open "https://skim-app.sourceforge.io/"
    ;;

# ---------------------------------------------------------------------

pkg-node|pkg-yarn|pkg-npm)
    "$InstFolder/pkg-node.sh" "$@"
    ;;

pkg-osx)
    "$InstFolder/pkg-osx.sh" "$@"
    ;;

# unrecognised command
*)
    usage "Unknown command: $Command"

esac
