
#==================================================
# @title        bashlib.sh
# @author       Jonathan Hadida
# @contact      Jhadida87 [at] gmail
#==================================================

source "$( dirname "${BASH_SOURCE}" )/jhenv.sh"
source "$JH_SRC/bash/include.sh"
