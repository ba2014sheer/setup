
#==================================================
# @title        jhenv.sh
# @author       Jonathan Hadida
# @contact      Jhadida87 [at] gmail
#==================================================

# set root folder
if [ -z "$JH_ROOT" ]; then
    JH_ROOT=$( cd "$( dirname "${BASH_SOURCE}" )/.." && pwd -P )
fi
if [ ! -d "$JH_ROOT" ]; then
    >&2 echo "[jhenv] Root folder not found: $JH_ROOT"
    return 1
fi
export JH_ROOT


# information about current shell
[ -n "$BASH_VERSION" ] && JH_FLAG_BASH=1
[ -n "$DISPLAY" ] && JH_FLAG_DISPLAY=1

export JH_FLAG_BASH
export JH_FLAG_DISPLAY

# information about the machine
if [[ $JH_FLAG_BASH ]]; then

    case $(uname -s) in
    Darwin)
        JH_OS='OSX'
        ;;
    Linux*)
        JH_OS='Linux'
        ;;
    esac
    export JH_OS

    export JH_HARDWARE=$(uname -m)
    export JH_CPU=$(uname -p)
    export JH_HOSTNAME=$(hostname)

fi

# relative paths
export JH_SRC="$JH_ROOT/src"
export JH_BIN="$JH_ROOT/bin"
export JH_INC="$JH_ROOT/inc"
export JH_TPL="$JH_ROOT/tpl"

export JH_ETC="$JH_ROOT/etc"
export JH_USR="$JH_ROOT/usr"
export JH_VAR="$JH_ROOT/var"

# load user env
JH_getUserName() {
    local UserName="$JH_USR/.name"
    if [ -s "$UserName" ]; then 
        cat "$UserName"
    else
        >&2 echo "[jhenv] Missing user name: $UserName"
        echo 'none'
    fi
}
export -f JH_getUserName

JH_loadUserEnv() {
    local UserEnv="$JH_USR/env.sh"
    if [ -s "$UserEnv" ]; then 
        source "$UserEnv"
        export JH_FNAME JH_LNAME JH_EMAIL
    fi
}
export -f JH_loadUserEnv

# define temp dir
JH_TMP=/tmp/jh-setup
if [ -n "$TMPDIR" ]; then 
    JH_TMP=${TMPDIR%/}/jh-setup
fi
export JH_TMP

# define trash dir
JH_TRASH="${HOME}/.Trash"
if [ -d "${HOME}/.local/share/Trash" ]; then
    JH_TRASH="${HOME}/.local/share/Trash"
fi
export JH_TRASH

# create missing dirs
[ ! -d "$JH_TMP" ] && mkdir "$JH_TMP"
[ ! -d "$JH_TRASH" ] && mkdir "$JH_TRASH"
