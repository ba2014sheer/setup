
#==================================================
# @title        profile.sh
# @author       Jonathan Hadida
# @contact      Jhadida87 [at] gmail
#==================================================

source "$( dirname "${BASH_SOURCE}" )/jhenv.sh"
source "$JH_SRC/profile/include.sh"

# include binaries
path_prepend "$JH_BIN"
path_prepend "$JH_USR/bin"

# include python lib
pypath_append "$JH_SRC/python"

# check for updates
#"${JH_SRC}/update.sh"
