
# source bash utils
source "$( dirname "${BASH_SOURCE}" )/bashutils.sh" || { echo "Could not source bashutils"; exit 1; }

# common checks needed in most scripts
chkfile() { [ -f "$1" ] || [ -L "$1" ] || echoerrx1 "File not found: $1"; }
chkexec() { [ -x "$1" ] || echoerrx1 "File not executable: $1"; }
chkdir() { [ -d "$1" ] || echoerrx1 "Folder not found: $1"; }
chkvar() { [ -n "${!1}" ] || echoerrx1 "Variable required: $1"; }
chkfun() { [[ $(type -t "$1") == 'function' ]] || echoerrx1 "Not a function: $1"; }
reqcmd() { [[ $(iscmd "$1") ]] || echoerrx1 "Command required: $1"; }

# mkdir checking if folder already exists, and exiting in case of failure
makedir() {
    local dpath=$1
    shift 1
    [ -d "$dpath" ] || \mkdir "$@" "$dpath" || echoerrx1 "Could not create folder: $dpath"
}

# fail messages with badge to identify provenance
FAIL_BADGE='main'
fail() {
    msg_bR "[$FAIL_BADGE] ERROR"
    echoerrx1 "$*"
}