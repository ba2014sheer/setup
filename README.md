
# Setup and management scripts

This mostly-bash library is designed to be "plug-n-play"; it doesn't mess with the host system, you just source it and use it, and nothing breaks when you unplug it (well, apart from scripts that need it to run ofc).

It is compatible with both OSX and Linux (although [Homebrew](https://brew.sh/) is required on OSX), and has been tested on:

 - OSX Sierra, High Sierra, Mojave
 - Linux CentOS, RedHat and Ubuntu


### What's in the box?

In short, a set of bash scripts and functions to help with many practical things:

 - Colour display, because life is sad without [colours](./src/bash/colors.sh);
 - Sensible shell [options](./src/profile/options.sh), [configuration](./src/profile/config.sh), and [completion](./src/profile/completion.sh);
 - Tools to help with [file manipulations](./src/bash/util_file.sh), [find & replace](./src/bash/util_find.sh), [encryption](./src/bash/app_crypto.sh), [compression](./src/bash/util_archive.sh), etc.
 - Tools to help with applications like [git](./src/bash/util_repo.sh), [PDFs](./src/bash/app_pdf.sh), [LaTeX](./src/bash/app_tex.sh), etc.
 - A pretty [prompt](./src/profile/prompt.sh), and a few [aliases](./src/profile/alias.sh).

It also contains extremely useful (at least to me!) scripts for:

 - Managing hundreds of code repositories, whether owned/forked or not, with/out wiki (see `jhr` and `~/.jh/usr/repo`);
 - Managing dozens of SSH hosts, multiple GitHub accounts, etc. (see `jhs` and `~/.jh/usr/host`);
 - Synchronising dozens of data sources (e.g. data folders on clusters) (see `jhd` and `~/.jh/usr/data`);
 - Installing essential development tools (e.g. conda, workbench, FSL, OSL, node.js), see `jhinst`.


### Installing it

Open a terminal, and choose where to store this package:
```bash
# this creates a folder ".jh/" in your home directory
# you can of course choose anything else
JH_ROOT=$HOME/.jh
```

Then go ahead and type:
```
git clone https://gitlab.com/jhadida/setup "$JH_ROOT"

pushd "$JH_ROOT"
./install
```

This should run through a few checks, and install basic software packages that are needed. 
If you are on Linux, you might need to install these packages yourself, and re-run the installer after that.


### Trying it

The installation script should give you instructions to load the library into your current shell, but should you need to load it manually:
```
source "$JH_ROOT/inc/profile.sh"
```

If you intend to use the management tools, you need to create your user space:
```
jhusr new <YourInitials>
```
where `<YourInitial>` should be e.g. `cn` for Chuck Norris. 
This will create a clean folder in `$JH_ROOT/usr` with the following contents:
```
usr/
├── app
|   User-specific application configuration and data.
|   For example, your LaTeX templates, or your vimrc.
|
├── bin
|   Utilities you wrote and want accessible by default in your shell.
|
├── data
|   Config files for the data-management tool: jhd
|
├── host
|   Config files for the host-management tool: jhs
|
├── repo
|   Config files for the repo-management tool: jhr
|
├── .name
|   Don't touch this. 
|   To change your username: jhusr rename NewName
|
└── env.sh
    Edit this file with vim by typing: jhusr conf
```

If you like the library, and would like to load it by default in your terminal, type:
```
jhtpl profile
```
This will copy a couple of files to your home (prefixed with `.jh-`), and tell you to add something to your `.bash_profile`.


### Updating/removing it

If the library is loaded in your shell, you can update it simply by typing:
```
jhup
```

If you run into issues, or don't like it anymore, or just want to take a break, then simply comment the lines you inserted in your `.bash_profile` and open a new terminal.


### Saving your user space

If you have installed the library and are using it, you might want to synchronise your user space with another computer.

To synchronise the folder `$JH_ROOT/usr`, you should create a repository for it, say on Gitlab (or Gihub, or Bitbucket).
Call it for example `setup-<YourInitials>`, then:
```
cd "$JH_ROOT/usr"
git init
git add .
git commit -am "Initial commit"
git remote add origin https://gitlab.com/<UserName>/setup-<YourInitials>.git
git push -u origin master
```

After that, you can install this library with your profile on a new machine, simply by calling the installer with the URL of the user repo:
```
./install https://gitlab.com/<UserName>/setup-<YourInitials>.git
```

See also the documentation of `jhusr`, particularly the subcommands `status` and `git`.


### What it can do

Many bash functions are defined in [`~/.jh/src/bash/`](./src/bash/), and can be used in the terminal by typing their names.

For example, let's search for file-tests `[ -f` in that folder:
```
qpushd ~/.jh/src/bash
fstr "\[ -f" -r --include=*.sh
qpopd
```

The `qpushd` and `qpopd` commands are just silent versions of [`pushd`/`popd`](https://en.wikipedia.org/wiki/Pushd_and_popd).
The `fstr` command searches files (option `-r` to recurse subfolders) for a [regular-expression](http://www.gnu.org/software/grep/manual/html_node/Regular-Expressions.html) using [`grep`](https://en.wikipedia.org/wiki/Grep): this is why we had to escape the bracket in `\[`. There is also a command called `frep` which allows you to search & replace; go ahead and type `frep` in your terminal, to see how it is used. 

Aside from all these functions, thee are also several powerful utilities to manage data, repositories, and SSH hosts:

 - Type `jhr` to see the doc for the repo manager;
 - Type `jhd` for the data manager;
 - Type `jhs` for the host manager.

 There are also a couple of other goodies:

 - Type `jhinst` to see the programs that can be installed;
 - Type `jhtpl` to setup templates from [`~/.jh/tpl`](./tpl/).

Unfortunately, there is no documentation for these tools right now (although they have a pretty helpful helptext), but that will come in due course. In the meantime, please ask me directly if you'd like to use those.

In any case, please **DO NOT** edit the files in `~/.jh/usr/*`; if you were clever enough to figure out that this is where the "managing" tools look for repositories/data/hosts, and want to define your own, simply type: 
```
jhusr new   <YourInitials>    (or a short name, for example "chn" for Chuck Norris)
jhusr load  <YourInitials>
```
This will move the existing configuration somewhere safe, and create new empty folders for your own stuff (it's necessary to keep the existing one for updating the repo).


### Exploring the directory

```
.
├── bin
|   This is where the main tools are: repo, host and data management FTW.
|
├── doc
|   Future documentation (work in progress).
|
├── etc
|   Config stuff, useless for now.
|
├── inc
|   To be included in your scripts (e.g. using source).
|
├── src
|   Actual code for the various tools, plus some extra Python goodies.
|
├── tpl
|   Generic templates to help coding stuff.
|
├── usr
|   Where your stuff goes!
|
├── var
|   Working directory for the management tools, where everything gets stored and organised.
|   This folder can become quite heavy.
|
├── install
|   Your guess is as good as mine..
|
├── README.md
├── search
└── todo.md
    Don't worry about these.
```
