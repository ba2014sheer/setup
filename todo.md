
## Vault

Would be good to create a vault folder, which is not tracked by git:
 - add command to "encrypt" that folder into a file "vault.enc", which is tracked;
 - add command to "decrypt" file into folder, overwriting previous contents;
 - link to elements in the vault from other places in the repo.

## URLs

Better to move URLs from `jhinst` into new utility `jhurl`?
Sounds rather like a bustom bin..
